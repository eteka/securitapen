A- SERVICES DE CRÉATION GRAPHIQUE

1- Conception d'Affiche HD
		
   * Tarif ordindaire     :10.000F
   * Tarif en lot(min:4)  : 5.000F

2- Visuels pour Imprimerie
		* Impression sur papier : 5.000F
		* Impression sur Bâche : 10.000F

B- SERVICES DE COMMUNICATION DIGITALE

	* A partir de 100.000F (Durée: 15 jours)
	- Création de page Facebook
	- Logo 
	- 15 Visuels
	- 1 Publication par jours
	- Suivie des abonnés (1000 abonnés en moyenne) 

	* A partir de 180.000F (Durée: 15 jours)
	- Création de page Facebook
	- Logo 
	- 40 Visuels
	- 3 Publication par jours
	- Suivie des abonnés (2000 abonnés en moyenne) 


C- SERVICES DE SITE WEB

	* Site Web Vitrine : à partir de 300.000F
	* Site Web avec Auto-Administration  : à partir de 450.000F

C- SERVICES DE PACK DE CAMPAGNE ÉLECTORALE

	* Pack silver  : 200.000F
	- Création de page Facebook, Twitter
	- Logo 
	- 2 Vidéos de campagne
	- 50 Visuels
	- 1 Direct 
	- 4 Publication par jours (2 sur Facebook et 2 sur Twitter)
	- Suivie des abonnés (1500 abonnés en moyenne) 

	* Pack Gold  : 350.000F
	- Création de page Facebook, Twitter
	- Logo 
	- 4 Vidéos de campagne
	- 60 Visuels
	- 2 Direct 
	- 6 Publication par jours (2 sur Facebook et 2 sur Twitter)
	- Suivie des abonnés (2500 abonnés en moyenne) 
		


