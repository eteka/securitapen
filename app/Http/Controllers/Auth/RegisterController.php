<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'min:3','max:150','alpha'],
            'email' => ['required', 'string', 'email', 'max:150', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $psdo=$this->generatePseudo($data['name']);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'pseudo' => $psdo,
            'password' => Hash::make($data['password']),
        ]);
    }
    public function generatePseudo( $var )
    {
        $exp=explode(' ',$var);
        $pseudo=$p=isset($exp[0])?$exp[0]:srand(10);
        $t=1;$i=0;
        $compteur=0;
        do {

            if($i>0){ $pseudo=$p.$i;}
            $compteur= User::where('pseudo',$pseudo)->count();
            $i++;
        } while ($compteur >0);
        return strtolower($pseudo);
    }
}
