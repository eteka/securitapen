<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Modeacquisition;
use Illuminate\Http\Request;
use Auth;

class ModeacquisitionController extends Controller
{
    protected  $perPage = 10;
    protected $table_name="mode_acquisitions";

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
   
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $order=strtolower($request->get('orderBy'))=="asc"?"asc":"desc";
        $perPage = $this->perPage;
        $total=Modeacquisition::select('id')->count();
        
        if (!empty($keyword)) {
            $modeacquisition = Modeacquisition::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orderBy("id",$order)->paginate($perPage);
        } else {
            $modeacquisition = Modeacquisition::orderBy("id",$order)->paginate($perPage);
        }

        return view('admin.modeacquisition.index', compact('modeacquisition','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.modeacquisition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'required|max:200|unique:'.$this->table_name.',nom'
		]);
        $requestData = $request->all();
        $requestData['user_id']=Auth::user()->id;
        Modeacquisition::create($requestData);

        return redirect('admin/modeacquisition')->with('success', __("Sauvegarde effectuée !"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $modeacquisition = Modeacquisition::findOrFail($id);

        return view('admin.modeacquisition.show', compact('modeacquisition'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $modeacquisition = Modeacquisition::findOrFail($id);

        return view('admin.modeacquisition.edit', compact('modeacquisition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'nom' => 'required|max:200|unique:'.$this->table_name.',nom'
		]);
        $requestData = $request->all();
        
        $modeacquisition = Modeacquisition::findOrFail($id);
        $modeacquisition->update($requestData);

        return redirect('admin/modeacquisition')->with('info', __("Mis à jour effectuée"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Modeacquisition::destroy($id);

        return redirect('admin/modeacquisition')->with('danger',  __("Suppression effectuée"));
    }
}
