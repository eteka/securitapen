<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TypeTransfert;
use Illuminate\Http\Request;

class TypeTransfertController extends Controller
{
    protected  $perPage = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $order=strtolower($request->get('orderBy'))=="asc"?"asc":"desc";
        $perPage = $this->perPage;
        $total=TypeTransfert::select('id')->count();
        
        if (!empty($keyword)) {
            $typetransfert = TypeTransfert::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orderBy("id",$order)->paginate($perPage);
        } else {
            $typetransfert = TypeTransfert::orderBy("id",$order)->paginate($perPage);
        }

        return view('admin.type-transfert.index', compact('typetransfert','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.type-transfert.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'required|max:10',
			'description' => 'max:1000'
		]);
        $requestData = $request->all();
        
        TypeTransfert::create($requestData);

        return redirect('admin/type-transfert')->with('flash_message', __("Sauvegarde effectuée !"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $typetransfert = TypeTransfert::findOrFail($id);

        return view('admin.type-transfert.show', compact('typetransfert'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $typetransfert = TypeTransfert::findOrFail($id);

        return view('admin.type-transfert.edit', compact('typetransfert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'nom' => 'required|max:10',
			'description' => 'max:1000'
		]);
        $requestData = $request->all();
        
        $typetransfert = TypeTransfert::findOrFail($id);
        $typetransfert->update($requestData);

        return redirect('admin/type-transfert')->with('flash_message', __("Mis à jour effectuée"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TypeTransfert::destroy($id);

        return redirect('admin/type-transfert')->with('flash_message',  __("Suppression effectuée"));
    }
}
