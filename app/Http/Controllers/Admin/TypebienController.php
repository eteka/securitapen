<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Typebien;
use Illuminate\Http\Request;
use Auth;
class TypebienController extends Controller
{
    protected  $perPage = 10;
    protected $table_name="typebiens";

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $order=strtolower($request->get('orderBy'))=="asc"?"asc":"desc";
        $perPage = $this->perPage;
        $total=Typebien::select('id')->count();
        
        if (!empty($keyword)) {
            $typebien = Typebien::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orderBy("id",$order)->paginate($perPage);
        } else {
            $typebien = Typebien::orderBy("id",$order)->paginate($perPage);
        }

        return view('admin.typebien.index', compact('typebien','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.typebien.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'required|max:200|unique:'.$this->table_name.',nom'
		]);
        $requestData = $request->all();
        $requestData['user_id']=Auth::user()->id;
        Typebien::create($requestData);

        return redirect('admin/typebien')->with('success', __("Sauvegarde effectuée !"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $typebien = Typebien::findOrFail($id);

        return view('admin.typebien.show', compact('typebien'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $typebien = Typebien::findOrFail($id);

        return view('admin.typebien.edit', compact('typebien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom' => 'required|max:200|unique:'.$this->table_name.',nom'
		]);
        $requestData = $request->all();
        
        $typebien = Typebien::findOrFail($id);
        $typebien->update($requestData);

        return redirect('admin/typebien')->with('info', __("Mis à jour effectuée"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Typebien::destroy($id);

        return redirect('admin/typebien')->with('danger',  __("Suppression effectuée"));
    }
}
