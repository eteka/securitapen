<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Favori;
use Illuminate\Http\Request;

class FavoriController extends Controller
{
    protected  $perPage = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $order=strtolower($request->get('orderBy'))=="asc"?"asc":"desc";
        $perPage = $this->perPage;
        $total=Favori::select('id')->count();
        
        if (!empty($keyword)) {
            $favori = Favori::where('user_id', 'LIKE', "%$keyword%")
                ->orWhere('bien_id', 'LIKE', "%$keyword%")
                ->orWhere('ref', 'LIKE', "%$keyword%")
                ->orderBy("id",$order)->paginate($perPage);
        } else {
            $favori = Favori::orderBy("id",$order)->paginate($perPage);
        }

        return view('favori.index', compact('favori','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('favori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required|max:10'
		]);
        $requestData = $request->all();
        
        Favori::create($requestData);

        return redirect('favori')->with('success', __("Sauvegarde effectuée !"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $favori = Favori::findOrFail($id);

        return view('favori.show', compact('favori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $favori = Favori::findOrFail($id);

        return view('favori.edit', compact('favori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required|max:10'
		]);
        $requestData = $request->all();
        
        $favori = Favori::findOrFail($id);
        $favori->update($requestData);

        return redirect('favori')->with('info', __("Mis à jour effectuée"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Favori::destroy($id);

        return redirect('favori')->with('danger',  __("Suppression effectuée"));
    }
}
