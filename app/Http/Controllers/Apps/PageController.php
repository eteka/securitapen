<?php

namespace App\Http\Controllers\Apps;

use App\Models\Cour;
use App\Models\Etablissement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     //   $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('web.index');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function condition()
    {
        return view('web.pages.condition');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reglement()
    {
        return view('web.pages.reglement');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cookies()
    {
        return view('web.pages.cookies');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function soumission()
    {
        return view('web.pages.soumission');
    }


      /**
      * Export all user data in to Ecel format.
      *
      * @return \Illuminate\Contracts\Support\Renderable , XLSX, CSV
      */
      public function export_all($id,$n=50,$orderBy='ASC'){
        $nlimit=(int)$this->getDblimite();
        $n=(int)$n<=0?$nlimit:$n;
        $biens=Auth::user()->biens()->get();//->toArray();

        if($biens->count()>$nlimit){
          $biens=$biens=Bien::where('user_id',Auth::user()->id)->limit($nlimit)->get();

        }
        //dd($biens);
        if(!$biens->count()){return back();}
        Excel::create('ALL', function($excel) use ($biens) {

          // Set the title
          $excel->setTitle(__('Exportation de mes biens'));

          // Chain the setters
          $excel->setCreator('SecuritAppend')
          ->setCompany('Odacesoft');
          $excel->setDescription(__('Fichier de mes biens par SécuritAppend'));
          $entete=[
            0=>__("Numéro de série"),
            1=>__("Nom"),
            2=>__("Visibilité"),
            3=>__("Mode d'acquisition"),
            4=>__("Catégorie"),
            5=>__("Marque"),
            6=>__("Modèle"),
            7=>__("Vendeur"),
            8=>__("Lieu d'achat"),
            9=>__("Prix d'achat"),
            10=>__("Information"),
            11=>__("Image")
          ];

          $excel->sheet('All', function($sheet) use ($entete,$biens)
          {

                //$sheet->fromArray($b);
                //dd($biens->get());

                $l=1;
                $tab=["A1","B1","C1","D1","E1","F1","G1","H1","I1","J1","K1","L1"];
                $j=0;
                foreach ($tab as $column) {
                  $v=isset($entete[$j])?$entete[$j]:'';

                  $sheet->cell($column, function($cell) use ($v) {
                      $cell->setValue($v);
                      $cell->setFontWeight('bold');
                      $cell->setAlignment('center');
                  });
              $j++;
            }
            #Set h
            $i=2;
            //$sheet->fromArray($biens,NULL, 'A2');

          foreach ($biens as  $b) {
              $mode=$b->mode?$b->mode->nom:"-";
              $type=$b->type?$b->type->nom:"-";
              $marque=$b->marque?$b->marque->nom:"-";
              $modele=$b->modele?$b->modele->nom:"-";
              $image=$b->media?$b->media->url:"-";
              $imei=$b->imei1;
              $imei=$b->imei2!=""?$imei.="-".$b->imei2:$imei;
              $imei=$imei!=""?"'".$imei."'":'-';
              $data=[
                $imei,
                $b->nom,
                $b->visibilite(), $mode,$type,
                $marque,$modele,$b->vendeur,
                $b->lieu_achat,$b->prix_achat,
                $b->infos,$image=="-"?$image:asset($image)
              ];

              $sheet->appendRow($i,$data);
              $i++;
            }
            $sheet->setAutoSize(true);
            $sheet->freezeFirstRow();
          });

          //})->download('csv');
        })->download('xlsx');
    }
    public function rcours($slug)
    {
        $dossier="files/cours/".$slug."/";
        //dd($dossier);
        if ($handle = opendir($dossier))
        {
            /* Create a new directory for sanity reasons*/
            $backup=$dossier.'backup/';
            if(!is_dir($backup))
            {
                mkdir($backup);
            }

            /*Iterate the files*/
            while (false !== ($file = readdir($handle)))
            {
                if ($file != "." && $file != "..")
                {
                   /* if(!strstr($file,"#SKU"))
                    {
                        continue; //Skip as it does not contain #SKU
                    }*/

                    $newf="fichier.pdf";
                    if($file!="backup"){
                        $info = pathinfo($file);
                        $ext = $info['extension'];
                        $newf = "cours_up_".str_slug($info['filename']).".".$ext;
                        //dd($backup.$newf);
                        copy($dossier.$file,$backup.$newf);
                    }


                    /*Remove the #SKU*/


                    /*Rename the old file accordingly*/
                    //rename("/path/to/images/" . $file,"/path/to/images/" . $newf);
                }
            }

            /*Close the handle*/
            closedir($handle);
        }
        return "Opération effectuée";
    }
    public function icours($slug)
    {
        //$dossiers = ["FASEG"=>'files/cours/FASEG',"FDSP"=> 'files/cours/FDSP'];// "FM"=>'files/cours/FM',
           // "IFSIO"=>'files/cours/IFSIO', "IUT"=>'files/cours/IUT'];
        //dd($dossier);
        //dd('');
        $k=$slug;

        $dossier='files/cours/'.$k;
        \Config::set('excel.csv.delimiter', ';');
        //foreach ($dossiers as $k=>$dossier) {

            $data=[];
            $data[]=[
                0=>__("Entité"),
                1=>__("fichier"),
                2=>__("titre"),
                3=>__("format")
            ];
            $nom=$k;
            if ($handle = opendir($dossier)) {
                /*Iterate the files*/

                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != "..") {
                        $info = pathinfo($file);

                        $fichier=$info['basename'];
                        $titre=str_replace("cours_up_","",$info['filename']);
                        $titre=ucfirst(str_replace("-"," ",$titre));
                        $titre=str_replace("_"," ",$titre);
                        //dd($titre);
                        $format=isset($info['extension'])?$info['extension']:'-';
                        if($format!="-") {
                            $d = [
                                $k, $fichier, $titre, $format
                            ];
                            $data[]=$d;
                        }
                        $fac=Etablissement::select("id")->where('sigle',$k)->first();
                        if($fac) {
                            $cours = [
                                'titre' => $titre, 'enseignants' => "", 'assistants' => "",
                                'fichier' => $dossier.'/'.$fichier, 'annee_acad' => "2019-2020",
                                'faculte' =>  $fac->id, 'filiere' => "",
                                'domaine' => "", 'mention' => "",
                                'grade' => "LICENCE",
                                'ecu' => "ECU", 'user_id' => 1
                            ];
                            try{
                               // dd('d');
                                Cour::create($cours);
                            }catch (\Exception $e){
                                dd($e->getMessage());
                            }

                            //dd($cours);
                        }
                      //dd('non!');
                    }

                }

                Excel::create(substr(str_slug($k),0,10), function($excel) use ($k,$data) {
                    //$info = pathinfo($file);
                    $excel->sheet(substr(str_slug($k),0,10), function ($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->export('csv');//,storage_path('files'));
                /*Close the handle*/
                closedir($handle);
            }

        //}
        return "Opération effectuée";
    }

}
