<?php


namespace App\Http\Controllers\Apps;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfilController  extends Controller
{

    private $nb_profil_bien=10;
    /**
     * Create a new controller instance:Auth Middleware.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show Notification web page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function notification(Request $request)
    {

        return view('web.profil.notification');
    }
    /**
     * Show user profil page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profil(Request $request,$id)
    {
        $biens=Auth::user()->biens()->paginate($this->nb_profil_bien);
        return view('web.profil.profil',compact("biens"));
    }
    /**
     * Show page to setting user profil.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit_profil (Request $request)
    {
        return view('web.profil.edit_profil');
    }
    /**
     * Show page to setting user profil.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profil_settings(Request $request)
    {
        return view('web.profil.profil_settings');
    }
    /**
     * Show page to setting user profil.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function help_center(Request $request)
    {
        return view('web.help.center');
    }
    /**
     * Show page to setting laguage and hours of user.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function lang_settings(Request $request)
    {
        return view('web.profil.lang_settings');
    }
    /**
     * Show page to setting account security setting of user.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function account_security(Request $request)
    {
        return view('web.profil.account_security');
    }

    /**
     * Show page to account setting of bussiness account.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function account_business(Request $request)
    {
        return view('web.profil.account_business');
    }

}