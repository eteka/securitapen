<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config; 
use Illuminate\Support\Facades\Redirect; 
use Illuminate\Support\Facades\Session;

class LangueController extends Controller
{
    /*
    * Cette fonction permet de changer la langue de l'application
    */
    public function ChangerLang($lang,Request$request)
    {
        $continue_url=$request->get("c_url");
        /*$locale = in_array($lang, config('app.locales')) ? $lang : config('app.fallback_locale');
        session(['locale' => $locale]);
        return back();*/
        $locale = in_array($lang, config('app.locales')) ? $lang : config('app.fallback_locale');
        session(['locale' => $locale]);
        $prev=url()->previous();
        $prev_tab=[url('lang/fr'),url('lang/en')];


        if($continue_url==NULL) {
            if (in_array($prev, $prev_tab)) {
                return \redirect()->to('login');

            }

            return \redirect()->intended($prev);
        }else{
          return  \redirect()->to(url($continue_url));
        }
    } 
}
