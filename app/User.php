<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','pseudo','photo_profil_id', 'photo_couverture_id','poste', 'provider', 'provider_id', 'password','etat','type_compte_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','etat','type_compte_id'
    ];

     /**
    * The database default colomn.
    *
    * @var Array
    */
    protected $psdo;
    
    protected $attributes=[
        "photo_profil_id"=>0,
        "photo_couverture_id"=>0,
        "etat"=>1,
        "provider"=>'0',
        "provider_id"=>0,
        "type_compte_id"=>1,
        "pseudo"=>"dnzdjdzjdz",
      ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getPseudo($n)
    {
       return srand($n);
    }
    public function biens()
    {
      return $this->hasMany("App\Models\Bien",'user_id','id');
    }
     /**
     * Les biens favoris de l'utilisateur
     */
    public function biensFavoris()
    {
        return $this->belongsToMany('App\Models\Bien',"user_bien_favoris","user_id",'bien_id')->withTimestamps();
    }
}
