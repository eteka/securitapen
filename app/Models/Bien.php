<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bien extends Model
{
  use SoftDeletes;
       /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'biens';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom','mode_acquisition_id','typebien_id','lieu_achat','visibilite','media_id','vendeur', 'date_achat','imei1','imei2','prix_achat','infos','marque_id','modele_id','user_id','devise_id','photo','photo_mini'];
    
    public function user()
    {
      return $this->belongsTo("App\User",'user_id');
    }
     /**
     * Les biens favoris de l'utilisateur
     */
    public function userFavoris()
    {
        return $this->belongsToMany('App\Models\Bien',"user_bien_favoris",'bien_id',"user_id")->withTimestamps();
    }

    public function media()
    {
        return $this->belongsTo("App\Models\Media",'media_id');
    }
    public function devise()
    {
        return $this->belongsTo("App\Models\Devise",'devise_id');
    }
    public static function table()
    {
      return $this->table;
    }
    
    public function mode()
    {
      return $this->belongsTo("App\Models\ModeAcquisition",'mode_acquisition_id');
    }
    public function type()
    {
      return $this->belongsTo("App\Models\Typebien",'typebien_id');
    }
    
    public function marque()
    {
      return $this->belongsTo(\App\Models\Marque::class,'marque_id');
      //return $this->belongsTo("App\Models\Marque",'marque_id');
    }
    public function modele()
    {
      return $this->belongsTo("App\Models\Modele",'modele_id');
    }
    public function visibilite()
    {
      $r='-';
      if($this->visibilite=="PRIVE"){$r=__("Privée");}
      if($this->visibilite=="PUBLIC"){$r=__("Public");}
      return $r;
    }
    public function prix_achat()
    {      
      return $this->prix_achat>0?$this->prix_achat:__("Gratuit");
    }
  }
