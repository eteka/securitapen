<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Typebien extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'typebiens';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'description', 'user_id'];

     /**
      * @param null
      * Recupérer tous les biens ayant ce modèle.
      * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
      */
    public function biens()
    {
        return $this->hasMany('App\Models\Bien');
    }
    /**
     * @param null
     * Recupérer tous les utilisateurs ayant des biens de ce type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
