<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Devise extends Model
{
  use SoftDeletes;
     
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devises';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['devise', 'description',"sigle", 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
