<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Marque extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'marques';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'description', 'user_id'];

     /**
     * Recupérer tous les biens ayant ce modèle.
     */
    public function biens()
    {
        return $this->hasMany('App\Models\Bien');
    }
    /**
     * Recupérer tous les utilisateurs ayant des biens de cette marque
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    
}
