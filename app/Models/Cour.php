<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cour extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cours';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['titre', 'enseignants', 'assistants', 'fichier', 'annee_acad', 'faculte', 'filiere', 'domaine', 'mention', 'grade', 'ecu', 'user_id'];

    
}
