@extends('layouts.app')

@section('title')
    {{__('Sécurité et surveillance')}} -
    {{Auth::user()->name." - "}}

@endsection

@section('nav')
    @include('layouts.partials.simple-nav-dark',['menu'=>"notification"])
    @include('apps.includes.nav.notification')
@endsection
@section('footer')

@endsection

@section('content')
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-9 hidden-xs" id="left">

                    <div class="main-section">


                    </div>
                </div>

                <div   class="col-md-3 hidden-xs  minh">
                    <div id="right">

                        <div  class="mtop15 text-sm text-muted mbottom10">
                            <span class="icon-tag "></span> Des Boutiques à découvrir
                        </div>
                        <div class=" sec_cardbgwhiteshadow3noborderrond5">

                            <div>
                                <img class="img-responsive rond5" src="{{asset('storage/statics/pub/affiche-1.jpg')}}" alt="Publicité pate dentifrice">
                            </div>
                        </div>
                        @include('layouts.partials.mini-footer')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
