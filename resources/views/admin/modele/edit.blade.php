@extends('layouts.app')
@section('title',__('Edition  Modele '))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')
   <div class="container minh">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-8">
                 <div class="row">
                    <div class="col-md-12">
                        <div class="sub_main_menu">
                        @section('main_message')
                        <div class="card-header">Edit  Modele #{{ $modele->id }}</div>
                        @endsection
                        </div>
                   </div>                    
                </div>
                <div class="card">
                    <div class="sec_bg-info">
                        <div class="card-body">
                            <h3 class="Subhead-heading ">{{__("Edition")}} Modele</h3>
                            
                            <p class="Subhead-description text-sm text-muted">
                            {{__("Modele.edit_message")}} <a href="{{route("help_apps")}}"><i class="icon-info" aria-hidden="true"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/modele') }}" title="Back"><button class="btn btn-link btn-sm"><i class="fa fa- icon-arrow-left" aria-hidden="true"></i>{{_('Retour')}} </button></a>
                        

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($modele, [
                            'method' => 'PATCH',
                            'url' => ['/admin/modele', $modele->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.modele.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
