@extends('layouts.app')
@section('title',__('Edition  Post '))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')
@include ('apps.includes.nav.top-slime-bar')
    <div class="container">
        
    </div>
    <style>
        .sub_main_menu{
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: start;
            -ms-flex-pack: start;
            justify-content: flex-start;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            padding: 5px 0;
        }
        .model_name{
            margin: 0;
            padding: 0 1rem 0 0;
            font-size: 1.62rem;
            font-weight: 500;
            color: #434349;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }
        .separateur-v{
            width: 1px;
            height: 22px;
            margin:0 10px;
            background:#dbdce7;
        }
        .core_searchgroup{
            display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
        }
        .form_searchgroup{
            margin-left: 20px!important;
        }
        .form_searchgroup .inner-search
        {
            width: 175px;
        display: inline-block;
        }
        .form_searchgroup .inner-search .form-control:focus
        {
            background:#FFFFFF;
            color:#454647;
            -webkit-box-shadow:0 0 3px inset #dededede;
            -moz-box-shadow:0 0 3px inset #dededede;
            box-shadow:0 0 3px inset #dededede;
        }
        .form_searchgroup .inner-search .form-control
        {
            background: #eaecf2;
            border-color: #eaecf2!important;
            font-size: 1.19rem;
            height: 33px;
            padding-right: 2.8rem;
        }
        

    .form_searchgroup .search_icon {
        position: absolute;
        height: 100%;
        margin-left:145px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        top: 0px;
        line-height:33px;
        width: 3.2rem;
        
    }
    .svg-icon {
        height: 23px;
        width: 23px;
        fill:#001122
    }
    .svg-icon g [fill] {
    fill: #244157;
}
    .right_main_menu{
display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: end;
    -ms-flex-pack: end;
    justify-content: flex-end;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 0;
    right:0;
    float:right;
    position:absolute;
}
.m-card table.table th{
    padding: 16px 10px;
    cursor:pointer;
    vertical-align: middle;
    padding: 15px 10px;
    font-size: 15px;
}
.m-card .table  tr>td{
    padding:10px;
}
.mtable .table > thead > tr > th,
.m-card .table > thead > tr > th,
.m-card .table  tr,
.m-card .table > tbody + tbody,
.m-card .table > tbody > tr > td
{
    border-bottom: 1px solid #f0f3ff;/* #e0e2ed;*/
}
    </style>
    <div class="container minh">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sub_main_menu">
                            <h4 class="model_name">Posts</h4>
                            <span class="separateur-v"></span>
                           
                           <div class="core_searchgroup">
                                <span>150 au total</span>
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/posts', 'class' => 'form_searchgroup', 'role' => 'search'])  !!}
                        
                           
                                <div class="inner-search">
                                    <input type="text" class="form-control" name="search"  value="{{ request('search') }}" class="form-control" placeholder="{{__('Rechercher...')}}" id="generalSearch">
                                    <span class="search_icon">
                                        <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
                                    </svg>                                    <!--<i class="flaticon2-search-1"></i>-->
                                </span>
                            </span>
                        </div>
                        </div>
                        {!! Form::close() !!}
                    
                    <div class="right_main_menu">
                    <a href="{{ url('/admin/posts/create') }}" class="btn btn-success btn-sm bold"><i class="sec_icon icon-plus"></i> Nouveau </a>
                    <div class="dropdown dropdown-inline" data-toggle="sec-tooltip-" title="Quick actions" data-placement="left">
                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-arrow-down"></i>                          <!--<i class="flaticon2-plus"></i>-->
                        </a>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
                            <!--begin::Nav-->
                            <ul class="sec-nav">
                               <li class="text-info"><b>Trier par ...</b></li>
                                <li class="sec-nav__item">
                                    <a href="#" class="sec-nav__link">
                                        <i class="icon-arrow-up-circle"></i>
                                        <span class="sec-nav__link-text">Plus récent</span>
                                    </a>
                                </li>
                                
                                <li class="sec-nav__item">
                                    <a href="#" class="sec-nav__link">
                                        <i class="icon-arrow-down-circle"></i>
                                        <span class="sec-nav__link-text">Plus ancien</span>
                                    </a>
                                </li>                        
                                
                                <li class="sec-nav__ separator"></li>
                                <li class="sec-nav__foot">
                                    <a class="bold" href="#">Exporter</a>                                    
                                </li>
                                <li class="sec-nav__foot">
                                    <a class="bold" href="#">Autres ...</a>                                    
                                </li>
                            </ul>
                            <!--end::Nav-->
                        </div>
                    </div>
                        </div>
                    </div>
                    </div>
                    </div>
                    <div>
                </div>
                <div class="card bgwhite m-card shadow3">
                   
                    <div class="pad0">
                       

                        <div class="table-responsive">
                            <table class="table table-borderless ">
                                <thead>
                                    <tr>
                                        <th class="pad10">#</th><th>Title</th><th>Content</th><th>Category</th><th >Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $item)
                                    <tr class="text-sm">
                                        <td ><div class="pad10">{{ $loop->iteration }}</div></td>
                                        <td>{{ $item->title }}</td><td>{{ str_limit($item->content,100) }}</td><td>{{ $item->category }}</td>
                                        <td nowrap>
                                        <div class="dropdown dropdown-inline" data-toggle="sec-tooltip-" title="Quick actions" data-placement="left">
                                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon-options"></i>                          <!--<i class="flaticon2-plus"></i>-->
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
                                            <!--begin::Nav-->
                                            <ul class="sec-nav">
                                            <li class="text-info"><b>Opérations ...</b></li>
                                                <li class="sec-nav__item">
                                                <a href="{{ url('/admin/posts/' . $item->id) }}" title="View Post">
                                                        <i class="icon-eye"></i>                                                        
                                                        <span class="sec-nav__link-text">{{__('Visualiser')}}</span>
                                                    </a>
                                                </li>
                                                <li class="sec-nav__item">
                                                <a href="{{ url('/admin/posts/' . $item->id . '/edit') }}" title="Edit Post">
                                                        <i class="icon-note"></i>                                                        
                                                        <span class="sec-nav__link-text">{{__('Modifier')}}</span>
                                                    </a>
                                                </li>
                                                <li class="sec-nav__item"> 
                                                    {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/posts', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            
                                            {!! Form::button('<i class="icon-trash" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-link bold text-danger',
                                                        'title' => 'Delete Post',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                                    {!! Form::close() !!}
                                                </li> 
                                            </ul>
                                            <!--end::Nav-->
                                        </div>
                                        </div>
                                            
                                            
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $posts->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
