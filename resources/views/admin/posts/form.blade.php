<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', trans('posts.title'), ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
    {!! Form::label('content', trans('posts.content'), ['class' => 'control-label']) !!}
    {!! Form::textarea('content', null, ('' == 'required') ? ['class' => 'form-control autosize', 'required' => 'required','rows'=>"4"] : ['class' => 'form-control autosize','rows'=>"4"]) !!}
    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
    {!! Form::label('category', trans('posts.category'), ['class' => 'control-label']) !!}
    {!! Form::select('category', json_decode('{"technology":"Technology","tips":"Tips","health":"Health"}', true), null, ('' == 'required') ? ['class' => 'form-control selectisize', 'required' => 'required'] : ['class' => 'form-control selectisize']) !!}
    {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::hidden('user_id', 1, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('Mettre à jour') : __('Sauvegarder'), ['class' => 'btn btn-primary']) !!}
</div>
