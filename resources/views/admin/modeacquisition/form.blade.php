<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', trans('modeacquisition.nom'), ['class' => 'control-label']) !!}
    {!! Form::text('nom', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('modeacquisition.description'), ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ('' == 'required') ? ['class' => 'form-control autosize', 'required' => 'required','rows'=>"4"] : ['class' => 'form-control autosize','rows'=>"4"]) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('Mettre à jour') : __('Sauvegarder'), ['class' => 'btn btn-primary']) !!}
</div>
