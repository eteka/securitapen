<div class="row">
    <div class="col-md-5">
        <div class="form-group {{ $errors->has('devise') ? 'has-error' : ''}}">
            {!! Form::label('devise', trans('devise.devise'), ['class' => 'control-label']) !!}
            {!! Form::text('devise', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('devise', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-3">

        <div class="form-group {{ $errors->has('sigle') ? 'has-error' : ''}}">
            {!! Form::label('sigle', trans('devise.sigle'), ['class' => 'control-label']) !!}
            {!! Form::text('sigle', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('sigle', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('devise.description'), ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ('' == 'required') ? ['class' => 'form-control autosize', 'required' => 'required','rows'=>"4"] : ['class' => 'form-control autosize','rows'=>"4"]) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('Mettre à jour') : __('Sauvegarder'), ['class' => 'btn btn-xs-block btn-primary']) !!}
</div>
