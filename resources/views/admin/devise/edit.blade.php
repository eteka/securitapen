@extends('layouts.app')
@section('title',__('Edition  Devise'))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')
   <div class="container minh">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-8">
                 <div class="row">
                    <div class="col-md-12">
                        <div class="sub_main_menu">
                        @section('main_message')
                        <div class="card-header">Edit  Devise #{{ $devise->id }}</div>
                        @endsection
                        </div>
                   </div>                    
                </div>
                <div class="card">
                    <div class="sec_bg-info">
                        <div class="card-body">
                            <h3 class="Subhead-heading ">{{__("Edition")}} Devise</h3>
                            
                            <p class="Subhead-description text-sm text-muted">
                            </p>
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/devise') }}" title="Back"><button class="btn btn-link btn-sm"><i class="fa fa- icon-arrow-left" aria-hidden="true"></i>{{_('Retour')}} </button></a>
                        

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($devise, [
                            'method' => 'PATCH',
                            'url' => ['/admin/devise', $devise->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.devise.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
