<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', trans('marque.nom'), ['class' => 'control-label']) !!}
    {!! Form::text('nom', null,  ['class' => 'form-control', 'required' => 'required']  !!}
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('marque.description'), ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null,  ['class' => 'form-control autosize', 'required' => 'required','rows'=>"4"]) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>



<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('Mettre à jour') : __('Sauvegarder'), ['class' => 'btn btn-primary']) !!}
</div>
