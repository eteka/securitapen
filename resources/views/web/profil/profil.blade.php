@extends('layouts.app')



@section('title')
    {{__('Profil')}} -
    {{Auth::user()->name." - "}}

@endsection

@section('nav')
    @include('layouts.partials.simple-nav-dark',['menu'=>"notification"])
    @include('apps.includes.nav.notification')
@endsection
@section('footer')

@endsection
<style> 
    #head-profil-cover{
        height: 260px !important;
        background: #ffffff;
        border: 0px solid #dfe5ea;
        border:0px solid #e5eff5;
        margin-top:10px;
    }
    #banner{
        height: 150px;
        background: #dfe8ec;
    }
    #top-profil-header{
        height: 125px;
        border-bottom: 0px solid  #dfe5ea;
        background: url("{{asset('assets/statics/app/pages/profil_bg_default.JPG')}}") center center #dfe5ea;
        border-radius: 3px 3px 0 0;
        
    }
    #user_profil_img{
        height: 100px;
    border-radius: 100px;
    position: absolute;
    left: 40px;
    margin-top: 75px;
    border: 3px solid #fff;
    }
    #profil_user_name{
        margin-top: 5px;
        margin-bottom: 0px;
        margin-left: 0px;
        font-weight: bold;
    }
    .ml130{margin-left: 130px}
    #bottom-profil-header{
        border:1px solid #e5eff5;
        height: 135px;
        border-radius: 0 0 3px 3px;
        padding: 0 10px 5px;
    }
    #user_stats>li{
        padding: 2px 10px;
        margin-top: 5px;
        border-right: 1px solid #e5eff5;
        height: 40px;
    }
    #user_stats{
       border-top: 1px solid #e5eff5;
    }
</style>
@section('content')
<div class="clear-top"> 
</div>
   <div class="main">
  <div class="container">
    <div class="row">
      <div class="col-md-2 hidden-xs" id="left">
        <div id="profil-stick">
          @include ('apps.includes.nav.user-profil')
        </div>
      </div>
      <div class="col-md-7 pad0">
        <div  id="head-profil-cover">
            <div id="top-profil-header">
                <!--img id="user_profil_img" src="{{asset('storage/statics/users/image-profile.jpg')}}" alt="ETEKA Wilfried"-->
            </div>
        <div id="bottom-profil-header" class="">
           <div class="row">
               <div class="col-md-7">
                    <h3 class="mtop50 bold " id="profil_user_name"><a href="">{{Auth::user()->name}}</a></h3>
            <div class="user_statut"><span class="text-sm text-muted">{{Auth::user()->poste}}</span></div>
            <div class="text-sm  btn-block text-muted mbottom5 mtop5">   
            <ul class="list-inline">
                <li>
                    <i class="icon-link"> </i><a class="text-muted" class="" href="{{url('/')}}">      {{Request::url()}}</a>
                </li>
                 <li>
                    <i class="icon-calendar"> </i><a class="text-muted" class="" href="{{url('/')}}">      {{Auth::user()->created_at->diffForHumans()}}</a>
                </li>
                <li>
                    <i class="icon-badge" title='{{__("Les badges décernés")}}'> </i><a class="text-muted" class="" href="{{url('/')}}">      Aucun</a>
                </li>
            </ul>
            </div>
               </div>
               <div class="col-md-5 text-right">   
                <ul class="list-inline mtop20">
                    <li>
                        <a href="" class="btn btn-success"> <i class="icon-like"> </i> S'abonner </a>
                    </li>
                     <li>
                        <a href="" class="btn btn-default"> <i class="icon-envelope"> </i>  Message </a>
                    </li>
                </ul>
               </div>
           </div>
            <div class="user_stats_cover">   
                        <ul class="list-inline text-sm" id="user_stats">
                            <li>
                                <p class="bold m0 item-header"> <i class="icon-bag"></i>   {{__('Biens')}}</p>  
                                <span class="text-muted">555</span>
                            </li>
                            <li>
                                <p class="bold m0 item-header"> <i class="icon-grid"></i>   {{__('Catégories')}}</p>  
                                <span class="text-muted">5</span>
                            </li>
                            <li>
                                <p class="bold m0 item-header"> <i class="icon-target"></i>   {{__('Marques')}}</p>  
                                <span class="text-muted">555</span>
                            </li>
                            <li>
                                <p class="bold m0 item-header"> <i class="icon-equalizer"></i>   {{__('Modèles')}}</p>  
                                <span class="text-muted">8</span>
                            </li>
                        </ul>
            </div>
            
        </div>

    </div>
        <div class="top_home_param_bloc pad10">
          <div class="row">
            <div class="col-xs-12 text-center-xs col-sm-3">
              @if(isset($count_bien) && $count_bien!=NULL)
              <span class="text-muted bold"><i class="icon-bag"></i> {{$count_bien}}</span>
              @endif
            </div>

            <div class="col-xs-12 text-center-xs mtop-xs-10 col-sm-9 text-right">
              {!! Form::open(['route' => 'dashboard', 'class' => 'form-horizontal', 'method' => 'get']) !!}
              <div class="top-line-options">
                <span><a href="{{route('bien_favoris')}}" class="btn btn-md btn-default"><i class="icon-star"></i></a></span>
                <span><a href="{{route('classement')}}" title="{{__('Classement')}}" class="btn btn-md btn-default"><i class="icon-organization"></i></a></span>
                <span>
                  <a title="{{__('Tout exporter')}}" href="{{route('export_all_biens')}}" class="btn btn-md btn-default">
                   <svg class="svg-icon svg-icon-md rotate180" viewBox="0 0 20 20">
                      <path fill="none" d="M8.416,3.943l1.12-1.12v9.031c0,0.257,0.208,0.464,0.464,0.464c0.256,0,0.464-0.207,0.464-0.464V2.823l1.12,1.12c0.182,0.182,0.476,0.182,0.656,0c0.182-0.181,0.182-0.475,0-0.656l-1.744-1.745c-0.018-0.081-0.048-0.16-0.112-0.224C10.279,1.214,10.137,1.177,10,1.194c-0.137-0.017-0.279,0.02-0.384,0.125C9.551,1.384,9.518,1.465,9.499,1.548L7.76,3.288c-0.182,0.181-0.182,0.475,0,0.656C7.941,4.125,8.234,4.125,8.416,3.943z M15.569,6.286h-2.32v0.928h2.32c0.512,0,0.928,0.416,0.928,0.928v8.817c0,0.513-0.416,0.929-0.928,0.929H4.432c-0.513,0-0.928-0.416-0.928-0.929V8.142c0-0.513,0.416-0.928,0.928-0.928h2.32V6.286h-2.32c-1.025,0-1.856,0.831-1.856,1.856v8.817c0,1.025,0.832,1.856,1.856,1.856h11.138c1.024,0,1.855-0.831,1.855-1.856V8.142C17.425,7.117,16.594,6.286,15.569,6.286z"></path>
                    </svg>
                  </a>
                </span>

                {!! Form::select('orderby',["asc"=>__("Plus récent"),"desc"=>__("Plus ancien")], app('request')->input('orderby'),['class' => 'btn-default text-md btn l-inline form-control-md', 'required' => 'required',"onchange"=>"this.form.submit()"]) !!}
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
        <div class="main-section">

          @if(isset($biens))
          @include('apps.includes.data.liste-biens',['biens'=>$biens,"icone"=>"icon-layers","msg"=>__('Aucun élément dans le classement')])
          @endif
        </div>
</div>

<div   class="col-md-3 hidden-xs  minh">
  <div id="right">
    <style>
    .lcad{
      box-shadow: 0 0 0 1px rgba(0,0,0,.15), 0 2px 3px rgba(0,0,0,.2);
      transition: box-shadow 83ms;
      background-color: #fff;
      border-radius: 2px;
      overflow: hidden;
      position: relative;
      height: 150px;
    }
    </style>
    <div  class="mtop15 text-sm text-muted mbottom10">
      <span class="icon-tag "></span> Des Boutiques à découvrir
    </div>
    <div class=" sec_cardbgwhiteshadow3noborderrond5">

      <div>
        <img class="img-responsive rond5" src="{{asset('storage/statics/pub/affiche-1.jpg')}}" alt="Publicité pate dentifrice">
      </div>
    </div>
    @include('layouts.partials.mini-footer')
  </div>
</div>
</div>
</div>
</div>

@endsection
