@extends('layouts.app')

@section('nav')
    @include('layouts.partials.darknav')
@endsection

@section('footer')
@include('layouts.partials.footer-fixeddark')
@endsection

@section('body')
<body class="bgsilver">
    <style>
/**************************************************
* FOOTER FIXED
***************************************************/
#footfixed{
    position: fixed;
    bottom: 0;
    margin-left: auto;
    margin-right: auto;
    padding: 0px 15px;
    background: rgba(0,0,0,.3);
    left: calc(50% - 20%);
}
@media screen and (max-width: 600px){
    .home-big-title{
        padding: 10px 0;
        margin-top: 15% !important;
        padding:  10px !important;
    }
    #footfixed{
        left: 0px !important;
    }
    .home-big-title>h1{
        font-size: 6vh !important;
    }
}
#footfixed .footfixed-links a{
    color: #d4d0d0
}
.navbar-default .navbar-collapse, .navbar-default .navbar-form{
    border-width: 0px !important;
}
#footfixed .footfixed-links a:hover{
    color: #eeeeee;
}

/***************************************************/

        .bg_home{
            background: url('{{asset("./assets/statics/app/pages/home_story.jpg")}}') no-repeat center center #244157;
            background-size: cover;
            height: 100%;
            position: absolute;
            width: 100%;
            top:0;
            right: 0;
            overflow: auto;
        }
        .home_backdrop{
            background:rgba(0,0,0,.574);
            color: #ffffff;
            position: fixed;
            overflow: auto;
            height: 100%;
            width: 100%;
        }
        .home-big-title a{
           // font-weight: 100;
            color: #eeeeee;
        }
        .home-big-title {
        text-align: center;
        padding: 15px;
        margin-top: 20%;

    }
    .nav-home .navbar-nav > li > a{
        color: #eeeeee;
    }
     .nav-home .navbar-nav > li > a:hover{color: #ffffff;}
    .home-big-title>h1{
        font-size: 7vh;
        font-weight: bold;
    }
    .btn-waze:hover{
        color: #ffffff;
        text-decoration: none;
    }
    .btn-waze {
    color: #eeeeee;
    background-color: #ff1866;
    /* border-color: #eea236; */
    font-weight: bold;
}
    </style>

@endsection

@section('content')
<div class="bg_home">
    <div class="home_backdrop">
<div class="container">
    <div class="row h-min ">
        <div class="col-md-8 minh col-md-offset-2">
            <div class="">
                <div class="home-big-title">
                    <h1>{{__("Sauvegarder vos objets,")}}
                        <br>
                        {{__('retrouvez-les en cas de perte.')}}
                    </h1>
                    <h4>{{__("La plateforme de sécurisation des biens et services qui ne ressemble à aucune autre.")}}</h4>
                    <a href="{{route('register')}}" class="btn mtop10 btn-waze btn-lg">{{__("Créer votre compte maintenant")}}</a>

                    <div class="mtop10">
                        {{__("S'inscrire avec")}} <a href=""><u>Gmail</u> </a>, <a href=""><u>Facebook </u></a> {{__('ou')}} <a href=""><u>LinkedIn </u></a>
                    </div>
               
                
                <div class=" text-center mtop30">                  
                 <a href="{{url('about')}}"> {{__("En savoir plus")}} <i class="icon-arrow-right"></i></a> 
                </div> 
            </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
