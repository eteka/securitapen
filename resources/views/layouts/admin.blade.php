<?php
$url = Request::path();
$tab_m = explode('/', $url);
$murl = (isset($tab_m[1])) ? $tab_m[1] : '/';
$hopital = \App\Models\Hopital::where('id', session('h_id'))->first();
if (!isset($hopital) || empty($hopital)) {
    echo "<a href='" . url('choose-hospital') . "'>Cliquez ici pour choisir un hôpital avant de continuer </a>";
    return;
}
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Bootstrap Admin App + jQuery">
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
        <title>@yield('title',"Administration IMU")</title>
        <!-- =============== VENDOR STYLES ===============-->
        <!-- FONT AWESOME-->
        <link rel="stylesheet" href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/app/css/bootstrap.css')}}" >
        <!-- SIMPLE LINE ICONS-->
        <link rel="stylesheet" href="{{asset('assets/vendor/simple-line-icons/css/simple-line-icons.css')}}">
        <!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="{{asset('assets/app/css/bootstrap.css')}}" >
        <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">-->

        <!-- =============== APP STYLES ===============-->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="{{asset('assets/app/css/app.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/wes.odace.css')}}" />
        <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"-->
        <link rel="stylesheet" href="{{asset('assets/app/css/theme-e.css')}}" >
        <!-- JQUERY-->
        <script src="{{asset('assets/vendor/jquery/dist/jquery-2.1.4.min.js')}}"></script>
        @yield('css')
        <style type="text/css">
            body{
                font-family: 13px/24px 'Arial','Lucida Sans Unicode', 'Lucida Grande', sans-serif;
                color: #222222 !important;
            }
            @media only screen and (min-width: 768px){
                .wrapper>footer {
                    background: white;
                }
            }
            div,div.panel{border-radius: 0}

        </style>
    </head>

    <body>

        @if(Auth::user())
        <div class="wrapper">

            <!-- top navbar-->

            {{--@if (Auth::user()->is_admin || Auth::user()->is_doctor)--}}
            <header class="topnavbar-wrapper">

                <!-- START Top Navbar-->
                <nav role="navigation" class="navbar topnavbar">
                    <!-- START navbar header-->
                    <div class="navbar-header">
                        <a href="{{url('/')}}" class="navbar-brand">
                            <div class="brand-logo">
                                <img src="{{asset('assets/app/kea-logo.png')}}" alt="App Logo" class="img-responsive">
                            </div>
                            <div class="brand-logo-collapsed">
                                <img src="{{asset('assets/app/kea-logo.png')}}" alt="App Logo" class="img-responsive">
                            </div>
                        </a>
                    </div>
                    <!-- END navbar header-->
                    <!-- START Nav wrapper-->
                    <div class="nav-wrapper">
                        <!-- START Left navbar-->
                        <ul class="nav navbar-nav">
                            <li>
                                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                                <a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" class="hidden-xs">
                                    <em class="icon-menu"></em>
                                </a>
                                <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                                <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
                                    <em class="icon-menu"></em>
                                </a>
                            </li>
                            <!-- START User avatar toggle-->
                            <li>
                                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                                <a id="user-block-toggle" href="#user-block" data-toggle="collapse">
                                    <em class="icon-user"></em>
                                </a>
                            </li>
                            <!-- END User avatar toggle-->
                            <li>
                                <a href="{{url('/choose-hospital')}}" title="Cliquez ici changer d'hôpital">
                                    <em class="fa fa-hospital-o"></em>
                                </a>
                            </li>
                            <!-- START lock screen-->
                            <li>
                                <a href="{{url('/logout')}}" title="Cliquez ici pour vous déconnecter">
                                    <em class="icon-lock"></em>
                                </a>
                            </li>
                            <!-- END lock screen-->
                        </ul>
                        <!-- END Left navbar-->
                        <!-- START Right Navbar-->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Search icon-->
                            @if (Auth::user()->is_admin)
                            <li>
                                <a href="#" data-search-open="">
                                    <em class="icon-magnifier"></em>
                                </a>
                            </li>
                            @endif
                            <!-- Fullscreen (only desktops)-->
                            <li class="visible-lg">
                                <a href="#" data-toggle-fullscreen="">
                                    <em class="icon-size-fullscreen"></em>
                                </a>
                            </li>
                            <!-- START Alert menu-->
                            <li class="dropdown dropdown-list">
                                <a href="#" data-toggle="dropdown">
                                    <em class="icon-bell"></em>
                                    <!--div class="label label-danger">11</div-->
                                </a>
                                <!-- START Dropdown menu-->
                                <ul class="dropdown-menu animatedflipInX">
                                    <li>

                                    </li>
                                </ul>
                                <!-- END Dropdown menu-->
                            </li>
                            <!-- END Alert menu-->
                            <!-- START Offsidebar button-->
                            <!--li>
                               <a href="#" data-toggle-state="offsidebar-open" data-no-persist="true">
                                  <em class="icon-notebook"></em>
                               </a>
                            </li-->
                            <!-- END Offsidebar menu-->
                        </ul>
                        <!-- END Right Navbar-->
                    </div>
                    <!-- END Nav wrapper-->
                    <!-- START Search form-->
                    <form role="search" method="GET"  action="{{url('administrator/search')}}" class="navbar-form" >
                        <div class="form-group has-feedback">
                            <input type="text" name="query" required="required" placeholder="Rechercher ..." class="form-control">
                            <div data-search-dismiss="" class="fa fa-times form-control-feedback"></div>
                        </div>
                        <button type="submit" class="hidden btn btn-default">Submit</button>
                    </form>
                    <!-- END Search form-->
                </nav>
                <!-- END Top Navbar-->
            </header>
           {{-- @endif
            @if (Auth::user()->is_admin || Auth::user()->is_doctor)--}}
            <!-- sidebar-->
            <aside class="aside">
                <!-- START Sidebar (left)-->
                <div class="aside-inner">
                    <style>
                        .category-content {
                            position: relative;
                            padding:15px 10px;
                            background: rgba(255,255,255,.1)
                        }
                        .media:first-child {
                            margin-top: 0;
                        }
                        .media, .media-body {
                            overflow: visible;
                        }
                        .media:first-child {
                            margin-top: 0;
                        }
                        .img-sm {
                            width: 36px !important;
                            height: 36px !important;
                        }
                        .media-body, .media-left, .media-right {
                            display: table-cell;
                            vertical-align: top;
                        }
                        .media-heading {
                            margin-bottom: 2px;
                            display: block;
                        }
                        .media-left, .media > .pull-left {
                            padding-right: 5px;
                        }
                        .media-heading {
                            margin-bottom: 2px;
                            display: block;
                            font-weight: bold;
                            white-space: nowrap;
                            overflow: hidden;
                            text-overflow: ellipsis;
                        }
                        .text-semibold {
                            font-weight: 500;
                        }
                        .sidebar-user {
                            position: relative;
                            padding-bottom: 8px;
                            color:#fefefe;
                        }
                        .text-size-mini {
                            font-size: 11px;
                        }
                    </style>
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                <a href="#" class="media-left"><img src="assets/app/admin/icones/nurse.png" class="img-circle img-sm" alt=""></a>
                                <div class="media-body">
                                    <span class="media-heading text-semibold">{{Auth::user()?Auth::user()->prenom.' '.Auth::user()->nom:NULL}}</span>
                                    <div class="text-size-mini text-muted">
                                        <i class="fa fa-building-o text-size-small"></i> &nbsp;{{$hopital->nom}}<br> <i class="fa fa-map-marker text-size-small text-info "></i> <span class="text-info">{{$hopital->ville}}</span>
                                    </div>
                                </div>

                                <div class="media-right media-middle">
                                    <ul class="icons-list">
                                        <li>
                                            <a href="#"><i class="icon-cog3"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav data-sidebar-anyclick-close="" class="sidebar">
                        <!-- START sidebar nav-->

                        <ul class="nav nav-pills nav-stacked">
                            <li ><a href="{{url('/')}}"><span class="icone"><img src="{{asset('assets/app/admin/menu/home.png')}}" ></span><span class="menu_name">Site Web</span></a></li>
                            <li class="{{(isset($murl) && $murl=='/')?'active':''}}"><a href="{{url('admin')}}"><span class="icone"><img src="{{asset('assets/app/admin/menu/dashboard.png')}}" ></span><span class="menu_name">Tableau de Bord</span></a></li>
                            <li class="{{(isset($murl) && ($murl=='accounts'|| $murl=='account'))?'active':''}}"><a href="{{route('indexAccounts')}}" class="left-tooltip" data-tooltip="Compte IMU" title="Compte IMU"><span class="icone"> <img src="{{asset('assets/app/admin/menu/account.png')}}"  ></span><span class="menu_name">Comptes</span></a></li>
                            <li class="{{(isset($murl) && ($murl=='hopitals' || $murl=='hopital'))?'active':''}}"><a href="{{url('admin/hopitals')}}" class="left-tooltip" data-tooltip="Hôpital" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/clinic.png')}}"  ></span><span class="menu_name">Hopitaux</span></a></li>									
                            <!--<li class="{{(isset($murl) && ($murl=='type-hopitals'))?'active':''}}"><a href="{{url('admin/type-hopitals')}}" class="left-tooltip" data-tooltip="Hôpital" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/clinic.png')}}"  ></span><span class="menu_name">Hopitaux</span></a></li>-->									
                            <li class="{{(isset($murl) && ($murl=='pharmacies'))?'active':''}}"><a href="{{url('admin/pharmacies')}}" class="left-tooltip" data-tooltip="Hôpital" title="Pharmacies"><span class="icone"> <img src="{{asset('assets/app/admin/menu/clinic.png')}}"  ></span><span class="menu_name">Pharmarcies</span></a></li>									

                            <li class="{{(isset($murl) && ($murl=='medecins'|| $murl=='doctor'))?'active':''}}"><a href="{{url('admin/medecins')}}" class="left-tooltip" data-tooltip="Médecin" title="Médecin"><span class="icone"> <img src="{{asset('assets/app/admin/menu/doctor.png')}}"></span><span class="menu_name">Médecins</span></a></li>									

                            <li class="{{(isset($murl) && ($murl=='patients'|| $murl=='patient'))?'active':''}}"><a href="{{url('admin/patients')}}" class="left-tooltip" data-tooltip="Patient hospitalisé" title="Patient hospitalisé"><span class="icone"> <img src="{{asset('assets/app/admin/menu/patient.png')}}" ></span><span class="menu_name">Patients</span></a></li>									
                            <li  class="{{(isset($murl) && ( $murl=='rendez-vous'))?'active':''}}"><a href="{{url('admin/rendez-vous')}}" class="left-tooltip" data-tooltip="Rendez-vous" title="Rendez-vous"><span class="icone"> <img src="{{asset('assets/app/admin/menu/rdv.png')}}"  ></span><span class="menu_name">Rendez-vous</span></a></li>	
                           <!--<li><a href="{{url('treatspatient')}}" class="left-tooltip" data-tooltip="Patient traité" title="Patient traité"><span class="icone"> <img src="{{asset('assets/app/admin/menu/treat_patient.png')}}"  ></span><span class="menu_name">Patient admis</span></a></li>-->									

                            <!--<li class="{{(isset($murl) && ($murl=='nurses' || $murl=='nurse'))?'active':''}}"><a href="{{route('indexNurses')}}" class="left-tooltip" data-tooltip="Infirmier" title="Infirmier"><span class="icone"> <img src="{{asset('assets/app/admin/menu/nurse.png')}}"  ></span><span class="menu_name">Infirmier</span></a></li>-->									

                            <!--<li class="{{(isset($murl) && ($murl=='supportstaffs' || $murl=='supportstaff'))?'active':''}}"><a href="{{route('indexStaffs')}}" class="left-tooltip" data-tooltip="Personnel " title="Personnel "><span class="icone"> <img src="{{asset('assets/app/admin/menu/support.png')}}" ></span><span class="menu_name">Personnels</span></a></li>-->									

                            <!--li class="{{(isset($murl) && ($murl=='pharmacists' || $murl=='pharmacist'))?'active':''}}">
                                <a href="{{route('indexPharmacists')}}" class="left-tooltip" data-tooltip="Pharmaciens" title="Pharmaciens"><span class="icone"> <img src="{{asset('assets/app/admin/menu/pharmacien.png')}}" ></span><span class="menu_name">Pharmaciens</span></a></li-->									

                            <li class="{{(isset($murl) && ($murl=='labo-staffs' || $murl=='labo-staff'))?'active':''}}">
                                <!--<a href="{{route('indexLStaffs')}}" class="left-tooltip" data-tooltip="Personnel de laboratoire" title="Personnel de laboratoire"><span class="icone"> <img src="{{asset('assets/app/admin/menu/laboratoire.png')}}" ></span><span class="menu_name">Personnel de laboratoire</span></a></li>-->									

                            <li class="{{(isset($murl) && ($murl=='accountants' || $murl=='accountant'))?'active':''}}" >
                                <!--<a href="{{route('indexAccountants')}}" class="left-tooltip" data-tooltip="Comptable" title="Comptable"><span class="icone"> <img src="{{asset('assets/app/admin/menu/accountant.png')}}"  ></span><span class="menu_name">Comptable</span></a></li>-->									



                            <!--<li><a href="{{url('treatment')}}" class="left-tooltip" data-tooltip="Traitement" title="Traitement"><span class="icone"> <img src="{{asset('assets/app/admin/menu/treatment.png')}}"  ></span><span class="menu_name">Traitement</span></a></li>-->									

                            <!--<li><a href="{{url('prescription')}}" class="left-tooltip" data-tooltip="Ordonnance" title="Ordonnance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/prescription.png')}}" ></span><span class="menu_name">Ordonnance</span></a></li>-->									
                            <li><a href="{{url('/admin/kea-qr-codes/')}}" class="left-tooltip" data-tooltip="Ordonnance" title="Ordonnance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/qrcode.png')}}" ></span><span class="menu_name">QR Code</span></a></li>									

                            <!--<li><a href="?dashboard=user&amp;page=bedallotment" class="left-tooltip" data-tooltip="Assignation Lit-Infirmiers" title="Assignation Lit-Infirmière"><span class="icone"> <img src="{{asset('assets/app/admin/menu/hospital_bed.png')}}" ></span><span class="menu_name">Assignation Lit</span></a></li>-->									

                            <!--li><a href="?dashboard=user&amp;page=operation" class="left-tooltip" data-tooltip="Liste des opérations" title="Liste des opérations"><span class="icone"> <img src="{{asset('assets/app/admin/menu/operation.png')}}"  ></span><span class="menu_name">Liste des opérations</span></a></li-->									

                            <!--<li><a href="?dashboard=user&amp;page=diagnosis" class="left-tooltip" data-tooltip="Diagnostic" title="Diagnostic"><span class="icone"> <img src="{{asset('assets/app/admin/menu/diagnostic.png')}}"  ></span><span class="menu_name">Diagnostic</span></a></li>-->									

                            <!--li><a href="?dashboard=user&amp;page=bloodbank" class="left-tooltip" data-tooltip="Banque de sang" title="Banque de sang"><span class="icone"> <img src="{{asset('assets/app/admin/menu/blood-bank.png')}}"  ></span><span class="menu_name">Banque de sang</span></a></li-->									





                            <!--li><a href="?dashboard=user&amp;page=event" class="left-tooltip" data-tooltip="Evénement" title="Evénement"><span class="icone"> <img src="{{asset('assets/app/admin/menu/notice.png')}}"  ></span><span class="menu_name">Evénement</span></a></li>									

                            <li><a href="?dashboard=user&amp;page=message" class="left-tooltip" data-tooltip="Message" title="Message"><span class="icone"> <img src="{{asset('assets/app/admin/menu/message.png')}}" pagespeed_url_hash="1100635938" ></span><span class="menu_name">Message</span></a></li>									

                            <li><a href="?dashboard=user&amp;page=ambulance" class="left-tooltip" data-tooltip="Ambulance" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/ambulance.png')}}"  ></span><span class="menu_name">Ambulance</span></a></li>									
                            <li><a href="?dashboard=user&amp;page=ambulance" class="left-tooltip" data-tooltip="Ambulance" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/gymnast.png')}}"  ></span><span class="menu_name">Centre de gymnastique</span></a></li>									
                            <li><a href="?dashboard=user&amp;page=ambulance" class="left-tooltip" data-tooltip="Ambulance" title="Ambulance"><span class="icone"> <img src="{{asset('assets/app/admin/menu/clinic.png')}}"  ></span><span class="menu_name">Clinique</span></a></li>									

                            <li><a href="?dashboard=user&amp;page=report" class="left-tooltip" data-tooltip="Rapport" title="Rapport"><span class="icone"> <img src="{{asset('assets/app/admin/menu/report.png')}}" pagespeed_url_hash="2238844459" ></span><span class="menu_name">Rapport</span></a></li-->									

                            <li><a href="?dashboard=user&amp;page=account" class="left-tooltip" data-tooltip="Mon compte " title="Mon compte"><span class="icone"> <img src="{{asset('assets/app/admin/menu/setting.png')}}"  ></span><span class="menu_name">Paramètres</span></a></li>									


                        </ul>
                        <ul class="nav hidden">
                            <!-- START user info-->
                            <li class="has-user-block">
                                <div id="user-block" class="collapse">
                                    <div class="item user-block">
                                        <div class="pull-right">
                                            <div class="btn-group dropdown">
                                                <button data-toggle="dropdown" class="btn btn-link">
                                                    <em class="fa fa-ellipsis-v fa-lg text-muted"></em>
                                                </button>
                                                <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                                    <li>
                                                        <a href="{{url('/profil')}}">
                                                            <span>Mon compte</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{url('/logout')}}">
                                                            <span>Déconnexion</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{url('/choose-hospital')}}">
                                                            <span>Changer d'hôpital</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- User picture-->
                                        <div class="user-block-picture">
                                            <div class="user-block-status">

                                                @php

                                                $u_img=Auth::user()->picture;
                                                $img='uploads/users/'.$u_img;
                                                if(!file_exists($img)){
                                                if(Auth::user()->sexe=='F'){
                                                $img="assets/statics/users/avatar-f.jpg";
                                                }else{
                                                $img="assets/statics/users/avatar-h.jpg";
                                                }
                                                }
                                                @endphp
                                                <img src="{{asset($img)}}" alt="Avatar" width="60" height="60" class="img-thumbnail img-circle">
                                                <div class="circle circle-success circle-lg"></div>
                                            </div>
                                        </div>
                                        <!-- Name and Job-->
                                        <div class="user-block-info">
                                            <span class="user-block-name">Salut, {{Auth::user()->prenom}}</span>
                                            <span >
                                                <a class="user-block-role" href="{{ url('/@'.Auth::user()->username.'/profil')}}">{{ "@".Auth::user()->username}}</a></span>


                                        </div>

                                    </div>
                                </div>
                            </li>
                            <!-- END user info-->
                            <!-- Iterates over all sidebar items-->



                            <a href="{{url('/administrator')}}" title="Tableau de bord" >
                                <!--div class="pull-right label label-info">3</div-->
                                <em class="icon-speedometer"></em>
                                <span>Tableau de bord</span>
                            </a>

                            </li>
                            <li class="nav-heading ">
                                <span data-localize="sidebar.heading.HEADER">Menu principal</span>
                            </li>
                            <li class="active">
                                <a href="#imu" title="Elements" data-toggle="collapse"  aria-expanded="false">
                                    <em class="icon-chemistry"></em>
                                    <span data-localize="sidebar.nav.element.ELEMENTS">Gestion Imu</span>
                                </a>
                                <ul id="imu" class="nav sidebar-subnav " aria-expanded="true" >
                                    @if (Auth::user()->is_admin)
                                    <li>
                                        <a href="{{url('admin/hopitals')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="fa fa-hospital-o"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Hopitaux</span>
                                        </a>
                                    </li>
                                    @endif
                                    @if (Auth::user()->is_admin || Auth::user()->is_doctor)
                                    <li>
                                        <a href="{{url('administrator/composants/consultations')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-folder"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Consultations</span>
                                        </a>
                                    </li>
                                    <!--li>
                                      <a href="{{url('administrator/composants/admissions')}}" >
                                    <!--div class="pull-right label label-success">30</div->
                                    <em class="icon-layers"></em>
                                    <span data-localize="sidebar.nav.WIDGETS">Hospitalisation</span>
                                    </a>
                              </li-->
                                    <li>
                                        <a href="{{url('administrator/composants/rendez-vous')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-clock"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Rendez vous</span>
                                        </a>
                                    </li>
                                    @endif
                                    <!--li>
                                      <a href="{{url('administrator/composants/treatments')}}" >
                                    <!--div class="pull-right label label-success">30</div->
                                    <em class="icon-folder"></em>
                                    <span data-localize="sidebar.nav.WIDGETS">Traitements</span>
                                    </a>
                              </li-->

                                </ul>
                            </li>
                            @if (Auth::user()->is_admin )
                            <li class=" ">
                                <a href="#users" title="Elements" data-toggle="collapse" class="text-danger collapsed" aria-expanded="false">
                                    <em class="icon-people "></em>
                                    <span data-localize="sidebar.nav.element.ELEMENTS">Utilisateurs</span>
                                </a>
                                <ul id="users" class="nav sidebar-subnav" aria-expanded="false" style="height: 0px;">
                                    <li>
                                        <a href="{{url('administrator/users/doctors')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-folder"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Médécins</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{url('administrator/users/patients')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-user"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Patients</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/users/patients')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-user"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Presonnels soignant</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/users/patients')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-user"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Agents du Ministère</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/users/roles')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-folder"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Type d'utilisateur</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/administrator/users/permissions')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-folder"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Permissions</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/users/permission-role')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-folder"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Premissions accordées</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('administrator/users/role-user')}}" >
                                            <!--div class="pull-right label label-success">30</div-->
                                            <em class="icon-folder"></em>
                                            <span data-localize="sidebar.nav.WIDGETS">Gestion des rôles</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li class="nav-heading ">
                                <span data-localize="sidebar.heading.COMPONENTS">Composants du site</span>
                            </li>
                            <li class="">
                                <a href="#elements" title="Elements" data-toggle="collapse" class="collapsed" aria-expanded="false">
                                    <em class="icon-chemistry"></em>
                                    <span data-localize="sidebar.nav.element.ELEMENTS">Elements</span>
                                </a>
                                <ul id="elements" class="nav sidebar-subnav collapse" aria-expanded="false" style="height: 0px;">
                                    <li class="sidebar-subnav-header">Contenu des pages</li>
                                    <li class=" ">
                                        <a href="{{url('administrator/imu/about')}}" title="Buttons">
                                            <span data-localize="sidebar.nav.element.BUTTON">A propos</span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="notifications.html" title="Notifications">
                                            <span data-localize="sidebar.nav.element.NOTIFICATION">Règles de confidentialité</span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="sweetalert.html" title="Sweet Alert">
                                            <span>Les incriptions</span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="tour.html" title="Tour">
                                            <span>Statistiques</span>
                                        </a>
                                    </li>

                                    <li class=" ">
                                        <a href="icons-weather.html" title="Weather Icons">
                                            <div class="pull-right label label-success">+100</div>
                                            <span data-localize="sidebar.nav.element.WEATHER_ICON">Messages</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                        <!-- END sidebar nav-->
                    </nav>
                </div>
                <!-- END Sidebar (left)-->
            </aside>
            <!-- offsidebar-->
            {{--@endif--}}

            <!-- Main section-->
            <section>
                <!-- Page content-->
                <div class="content-wrapper">

                   {{-- @if (Auth::user()->is_admin || Auth::user()->is_doctor)--}}
                    @yield('header') 
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-11 ">
                                @if(Session::has('flash_message')) 

                                <div class="alert padR0 mtop10 alert-success alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Information</strong> 
                                    <p>{{ Session::get('flash_message') }} </p>
                                </div>
                                @endif
                                @if(Session::has('success'))          
                                <div class="alert mtop10 alert-success alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Opération réussie</strong> 
                                    <p>{{ Session::get('success') }} </p>
                                </div>
                                @endif
                                @if(Session::has('danger'))          
                                <div class="alert mtop10 alert-danger alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Attention</strong> 
                                    <p>{{ Session::get('danger') }} </p>
                                </div>
                                @endif
                                @if(Session::has('warning'))          
                                <div class="alert mtop10 alert-warning alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Avertissement</strong> 
                                    <p>{{ Session::get('warning') }} </p>
                                </div>

                                @endif
                            </div>
                        </div>
                    </div>
                    @yield('content')
                    {{--@endif--}}

                </div>
            </section>
            <!-- Page footer-->
            <footer class="text-right text-sm">
                <span>&copy; KEA Medicals 2016 - {{date('Y')}} - Tous droits réservés</span>
            </footer>
        </div>

        <!-- =============== VENDOR SCRIPTS ===============-->
        <!-- AUTOSIZE-->
        <script src="{{asset('assets/vendor/autosize/autosize.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/app-admin.js')}}"></script>
        <!-- MODERNIZR-->
        <script src="{{asset('assets/vendor/modernizr/modernizr.custom.js')}}"></script>
        <!-- MATCHMEDIA POLYFILL-->
        <script src="{{asset('assets/vendor/matchMedia/matchMedia.js')}}"></script>

        <!-- BOOTSTRAP-->
        <script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
        <!-- STORAGE API-->
        <script src="{{asset('assets/vendor/jQuery-Storage-API/jquery.storageapi.js')}}"></script>
        <!-- JQUERY EASING-->
        <!--<script src="{{asset('assets/vendor/jquery.easing/js/jquery.easing.js')}}"></script>-->
        <!-- ANIMO-->
        <!--<script src="{{asset('assets/vendor/animo.js/animo.js')}}"></script>-->
        <!-- SLIMSCROLL-->
        <!--<script src="{{asset('assets/vendor/slimScroll/jquery.slimscroll.min.js')}}"></script>-->
        <!-- SCREENFULL-->
        <script src="{{asset('assets/vendor/screenfull/dist/screenfull.js')}}"></script>
        <!-- LOCALIZE-->
        <!--<script src="{{asset('assets/vendor/jquery-localize-i18n/dist/jquery.localize.js')}}"></script>-->
        <!-- RTL demo-->
        <!--<script src="{{asset('assets/app/js/demo/demo-rtl.js')}}"></script>-->
        <!-- =============== PAGE VENDOR SCRIPTS ===============-->
        <!-- SPARKLINE-->
        <!--<script src="{{asset('assets/vendor/sparkline/index.js')}}"></script>-->
        <!-- GOOGLE MAPS-->
        <!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
        <!--<script src="{{asset('assets/vendor/jQuery-gMap/jquery.gmap.min.js')}}"></script>-->
        <!-- =============== APP SCRIPTS ===============-->
        <script src="{{asset('assets/app/js/app.js')}}"></script>
        <script src="{{asset('assets/vendor/pjax/pjax.js')}}"></script>

        @yield('script')
        @yield('scripts')
        <!-- =============== VENDOR SCRIPTS ===============-->
        <!-- MODERNIZR-->
        <!--script src="../vendor/modernizr/modernizr.custom.js"></script>
        <!-- JQUERY->
        <script src="../vendor/jquery/dist/jquery.js"></script>
        <!-- BOOTSTRAP->
        <script src="../vendor/bootstrap/dist/js/bootstrap.js"></script>
        <!-- STORAGE API->
        <script src="../vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
        <!-- PARSLEY->
        <script src="../vendor/parsleyjs/dist/parsley.min.js"></script>
        <!-- =============== APP SCRIPTS ===============->
        <script src="js/app.js"></script-->
        @else
        @php
        redirect('/login');
        @endphp
        @endif
    </body>
</html>
