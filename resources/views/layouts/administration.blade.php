<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Bootstrap Admin App + jQuery">
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
        <link rel="SHORTCUT ICON" href="{{ asset('assets/favicon.ico') }}" />
        <link rel="shortcut icon" href="{{ asset('assets/favicon.ico') }}">
        <title>@yield('title',"Administration IMU")</title>

        @include('layouts.css_administration')
        @yield('css')

    </head>

    <body>

        @if(Auth::user())
            <div class="wrapper">
            <!-- top navbar-->
            <header class="topnavbar-wrapper">
                <!-- START Top Navbar-->
            @include('layouts.header.header_administration')
                <!-- END Top Navbar-->
            </header>
            <!-- sidebar-->
            <aside class="aside">
                <!-- START Sidebar (left)-->
                <div class="aside-inner">

                    <nav data-sidebar-anyclick-close="" class="sidebar">
                        <!-- START sidebar nav-->
                        @include('layouts.menu.administrationMenu')
                        <!-- END sidebar nav-->
                    </nav>
                </div>
                <!-- END Sidebar (left)-->
            </aside>
            <!-- offsidebar-->
            <!-- Main section-->
            <section>
                <!-- Page content-->
                <div class="content-wrapper">
                    @yield('header') 
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-11 ">
                                @if(Session::has('flash_message')) 

                                <div class="alert padR0 mtop10 alert-success alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Information</strong> 
                                    <p>{{ Session::get('flash_message') }} </p>
                                </div>
                                @endif
                                @if(Session::has('success'))          
                                <div class="alert mtop10 alert-success alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Opération réussie</strong> 
                                    <p>{{ Session::get('success') }} </p>
                                </div>
                                @endif
                                @if(Session::has('danger'))          
                                <div class="alert mtop10 alert-danger alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Attention</strong> 
                                    <p>{{ Session::get('danger') }} </p>
                                </div>
                                @endif
                                @if(Session::has('warning'))          
                                <div class="alert mtop10 alert-warning alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Avertissement</strong> 
                                    <p>{{ Session::get('warning') }} </p>
                                </div>

                                @endif
                            </div>
                        </div>
                    </div>
                    @yield('content')
                </div>
            </section>
            <!-- Page footer-->
            <footer class="text-right text-sm">
                <span>&copy; KEA Medicals 2016 - {{date('Y')}} - Tous droits réservés</span>
            </footer>
        </div>
        @else
            @php
                redirect('/login');
            @endphp
        @endif
        @include('layouts.jss_administration')
        @yield('script')
    </body>
</html>
