<style>

    .user-widget-username{
        font-size: small;
        font-weight: bold;
    }
    .user-widget-text{
        font-size: x-small;

    }

    /*
    .mih-10 {
        min-height: 10rem;
    }
    .pa7-ns {
        padding: 2.8rem;
    }
    .justify-between {
        -ms-flex-pack: justify;
        -webkit-box-pack: justify;
        justify-content: space-between;
    }
    .items-center {
        -ms-flex-align: center;
        -webkit-box-align: center;
        align-items: center;
    }
    .flex-auto {
        -ms-flex: 1 1 auto;
        -webkit-box-flex: 1;
        flex: 1 1 auto;
        min-height: 0;
        min-width: 0;
    }
    .flex {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
    }
    .b--whitegrey {
        border-color: #e5eff5;
    }
    .bb {
        border-bottom-style: solid;
        border-bottom-width: 1px;
    }

    .tc {
        text-align: center;
    }
    .mr5 {
        margin-right: 2rem;
    }
    .w7 {
        width: 2.8rem;
    }
    .dib {
        display: inline-block;
    }
    .f5-l {
        font-size: 1.45rem;
    }
    .mt0-l {
        margin-top: 0;
    }

    .mt2 {
        margin-top: .8rem;
    }
    .ma0 {
        margin: 0;
    }
    .pa0 {
        padding: 0;
    }
    .midgrey {
        color: #738a94;
    }
    .f8 {
        font-size: 1.3rem;
    }*/
    </style>
<div id="nav-show-bar" class="bgsilver">
<div class="container">
    <div class="row">
        <div class="col-md-12"></div>

        <div class="col-md-8">
            <div class="stitle mtop10"><a title="{{$bien->nom}}" href="{{route('show_bien',$bien->id)}}">   {{str_limit($bien->nom,100)}}</a></div>
            <div>
                <ul class="list-inline-point text-muted text-sm">
                    @if($bien->user)
                    <li>

                    <small class=""><i class="icon-user"></i>  <a href="{{route('user_profil',$bien->user->id)}}"> {{$bien->user?$bien->user->name:"-"}} </a> &nbsp;&nbsp;</small>
                    @endif
                    <li>
                            <small class=""><i class="icon-calendar"></i> {{__('Ajouté,')}} {{$bien->created_at->diffForHumans()}}</small>
                    </li>
                    @if($bien->created_at!=$bien->updated_at)
                    <li>
                        <small class=""><i class="icon-pencil"></i> {{__('Dernière mise à jour,')}} {{$bien->updated_at->diffForHumans()}}</small>
                    </li>
                    @endif
                </ul>

            </div>
        </div>
        <div class="col-md-4 text-right ">
          <ul class="list-inline mtop15">
            <li class="col-sm-3 col-xs-4 text-xs-left">
              <a  class="btn sm-btn-svg btn-sm btn-default bold btn-xs-block" href="{{route('export_bien',$bien->id)}}" title="{{_('Exporter')}}">
                  <svg class="svg-icon rotate180" viewBox="0 0 20 20">
                      <path fill="none" d="M8.416,3.943l1.12-1.12v9.031c0,0.257,0.208,0.464,0.464,0.464c0.256,0,0.464-0.207,0.464-0.464V2.823l1.12,1.12c0.182,0.182,0.476,0.182,0.656,0c0.182-0.181,0.182-0.475,0-0.656l-1.744-1.745c-0.018-0.081-0.048-0.16-0.112-0.224C10.279,1.214,10.137,1.177,10,1.194c-0.137-0.017-0.279,0.02-0.384,0.125C9.551,1.384,9.518,1.465,9.499,1.548L7.76,3.288c-0.182,0.181-0.182,0.475,0,0.656C7.941,4.125,8.234,4.125,8.416,3.943z M15.569,6.286h-2.32v0.928h2.32c0.512,0,0.928,0.416,0.928,0.928v8.817c0,0.513-0.416,0.929-0.928,0.929H4.432c-0.513,0-0.928-0.416-0.928-0.929V8.142c0-0.513,0.416-0.928,0.928-0.928h2.32V6.286h-2.32c-1.025,0-1.856,0.831-1.856,1.856v8.817c0,1.025,0.832,1.856,1.856,1.856h11.138c1.024,0,1.855-0.831,1.855-1.856V8.142C17.425,7.117,16.594,6.286,15.569,6.286z"></path>
              </svg></a>
            </li>
            <li class="col-sm-3 col-xs-4">
              <a class="btn btn-sm btn-default bold btn-xs-block" href="{{route('edit_bien',$bien->id)}}" title="{{_('Modifier')}}"><i class="icon-note"></i> <span class="hidden-xs_"> {{_('Modifier')}}</span> </a>

            </li>
            <li class="col-sm-3 col-xs-4">
              @if($bien->userFavoris && $bien->userFavoris->count())
            <div class="input-group  ">
              <a class="btn sbtn-btn-r btn-sm btn-default bold btn-xs-block" href="{{route('delete_bien_favori',$bien->id."?token=".sha1($bien->id))}}" title="{{_('Retirer de mes favoris')}}">
                  <span class="title-svg-icon" title="{{__('Ajouté aux favoris')}}">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/></svg>
                  </span>

              <span class="hidden-xs"> {{_('Retirer de mes favoris')}}</span> </a>

            <span class="input-group-addon sbtn-btn-info bold" id="basic-addon1">{{isset($nb_favoris)?$nb_favoris:0}}</span>
          </div>
            @else
              <div class="input-group ">
                <a class="btn btn-sm btn-default sbtn-btn-r bold btn-xs-block" href="{{route('add_bien_favori',$bien->id."?token=".sha1($bien->id))}}" title="{{_('Ajouter aux favoris')}}"><i class="icon-star"></i>
                    <span class="hidden-xs"> {{_('Ajouter aux favoris')}}</span> </a>

            <span class="input-group-addon sbtn-btn-info bold" id="basic-addon1">{{isset($nb_favoris)?$nb_favoris:0}}</span>
          </div>
            @endif
            </li>
          </ul>


        </div>
    </div>
</div>
</div>

<div class="main" id="main_show">
<div class="container">
    <div class="row">
        <div class="col-md-2 col-sm-12 col-xs-12">
            <!--div class="user-widget-content">
                <div class="user-widget-mini">
                    <img src="{{asset('storage/statics/default/users/100_4.jpg')}}" alt="">
                </div>
                <div class="user-widget-mini-info">
                    <a title="{{$bien->user?$bien->user->name:''}}" href="#" class="user-widget-username">
                        {{$bien->user?str_limit($bien->user->name,30):''}}
                    </a>
                    @if($bien->user && $bien->user->poste!=NULL)
                <p class="user-widget-text" title="{{$bien->user?$bien->user->poste:''}}">
                        {{$bien->user?str_limit($bien->user->poste,55):''}}
                    </p>
                    @else
                    <p class="user-widget-text">
                        {{__('Inscription,')}}{{$bien->user->created_at->diffForHumans()}}
                    </p>
                    @endif
                </div>
            </div-->
        </div>
        <div class="col-md-10">
            <ul class="nav nav-tabs" id="show-nav">
            <li role="presentation" class="{{isset($section) && $section=="element"?"active":''}}">
                    <a href="{{route('show_bien',$bien->id)}}">
                    <span class="sec-tabs-icon">
                    <svg class="svg-icon-l" viewBox="0 0 20 20">
                            <path d="M14.467,1.771H5.533c-0.258,0-0.47,0.211-0.47,0.47v15.516c0,0.414,0.504,0.634,0.802,0.331L10,13.955l4.136,4.133c0.241,0.241,0.802,0.169,0.802-0.331V2.241C14.938,1.982,14.726,1.771,14.467,1.771 M13.997,16.621l-3.665-3.662c-0.186-0.186-0.479-0.186-0.664,0l-3.666,3.662V2.711h7.994V16.621z"></path>
                        </svg>
                    </span>
                    <span class="tabs-info_">{{__('Élément')}} </span></a>
            </li>
            <!--li role="presentation" class="{{isset($section) && $section=="activite"?"active":''}}">
                <a href="{{route('show_activite_bien',$bien->id)}}">
                <span class="sec-tabs-icon">

                    <svg class="svg-icon-l" viewBox="0 0 20 20">
                        <path d="M10.25,2.375c-4.212,0-7.625,3.413-7.625,7.625s3.413,7.625,7.625,7.625s7.625-3.413,7.625-7.625S14.462,2.375,10.25,2.375M10.651,16.811v-0.403c0-0.221-0.181-0.401-0.401-0.401s-0.401,0.181-0.401,0.401v0.403c-3.443-0.201-6.208-2.966-6.409-6.409h0.404c0.22,0,0.401-0.181,0.401-0.401S4.063,9.599,3.843,9.599H3.439C3.64,6.155,6.405,3.391,9.849,3.19v0.403c0,0.22,0.181,0.401,0.401,0.401s0.401-0.181,0.401-0.401V3.19c3.443,0.201,6.208,2.965,6.409,6.409h-0.404c-0.22,0-0.4,0.181-0.4,0.401s0.181,0.401,0.4,0.401h0.404C16.859,13.845,14.095,16.609,10.651,16.811 M12.662,12.412c-0.156,0.156-0.409,0.159-0.568,0l-2.127-2.129C9.986,10.302,9.849,10.192,9.849,10V5.184c0-0.221,0.181-0.401,0.401-0.401s0.401,0.181,0.401,0.401v4.651l2.011,2.008C12.818,12.001,12.818,12.256,12.662,12.412"></path>
                    </svg>
                </span>
                <span class="tabs-info">{{__("Activités")}}</span></a>
            </li>
            <li role="presentation" class="{{isset($section) && $section=="mouvement"?"active":''}}">
                    <a href="{{route('show_mouvement_bien',$bien->id)}}">
                    <span class="sec-tabs-icon">
                    <svg class="svg-icon-l" viewBox="0 0 20 20">
                            <path d="M12.319,5.792L8.836,2.328C8.589,2.08,8.269,2.295,8.269,2.573v1.534C8.115,4.091,7.937,4.084,7.783,4.084c-2.592,0-4.7,2.097-4.7,4.676c0,1.749,0.968,3.337,2.528,4.146c0.352,0.194,0.651-0.257,0.424-0.529c-0.415-0.492-0.643-1.118-0.643-1.762c0-1.514,1.261-2.747,2.787-2.747c0.029,0,0.06,0,0.09,0.002v1.632c0,0.335,0.378,0.435,0.568,0.245l3.483-3.464C12.455,6.147,12.455,5.928,12.319,5.792 M8.938,8.67V7.554c0-0.411-0.528-0.377-0.781-0.377c-1.906,0-3.457,1.542-3.457,3.438c0,0.271,0.033,0.542,0.097,0.805C4.149,10.7,3.775,9.762,3.775,8.76c0-2.197,1.798-3.985,4.008-3.985c0.251,0,0.501,0.023,0.744,0.069c0.212,0.039,0.412-0.124,0.412-0.34v-1.1l2.646,2.633L8.938,8.67z M14.389,7.107c-0.34-0.18-0.662,0.244-0.424,0.529c0.416,0.493,0.644,1.118,0.644,1.762c0,1.515-1.272,2.747-2.798,2.747c-0.029,0-0.061,0-0.089-0.002v-1.631c0-0.354-0.382-0.419-0.558-0.246l-3.482,3.465c-0.136,0.136-0.136,0.355,0,0.49l3.482,3.465c0.189,0.186,0.568,0.096,0.568-0.245v-1.533c0.153,0.016,0.331,0.022,0.484,0.022c2.592,0,4.7-2.098,4.7-4.677C16.917,9.506,15.948,7.917,14.389,7.107 M12.217,15.238c-0.251,0-0.501-0.022-0.743-0.069c-0.212-0.039-0.411,0.125-0.411,0.341v1.101l-2.646-2.634l2.646-2.633v1.116c0,0.174,0.126,0.318,0.295,0.343c0.158,0.024,0.318,0.034,0.486,0.034c1.905,0,3.456-1.542,3.456-3.438c0-0.271-0.032-0.541-0.097-0.804c0.648,0.719,1.022,1.659,1.022,2.66C16.226,13.451,14.428,15.238,12.217,15.238"></path>
                        </svg>
                    </span>
                    <span class="tabs-info">{{__("Mouvements")}}</span> </a>
            </li>
            <li role="presentation" class="{{isset($section) && $section=="securite"?"active":''}}">
                <a href="{{route('show_securite_bien',$bien->id)}}">
                <span class="sec-tabs-icon">
                    <svg class="svg-icon-l" viewBox="0 0 20 20">
                        <path d="M12.546,4.6h-5.2C4.398,4.6,2,7.022,2,10c0,2.978,2.398,5.4,5.346,5.4h5.2C15.552,15.4,18,12.978,18,10C18,7.022,15.552,4.6,12.546,4.6 M12.546,14.6h-5.2C4.838,14.6,2.8,12.536,2.8,10s2.038-4.6,4.546-4.6h5.2c2.522,0,4.654,2.106,4.654,4.6S15.068,14.6,12.546,14.6 M12.562,6.2C10.488,6.2,8.8,7.904,8.8,10c0,2.096,1.688,3.8,3.763,3.8c2.115,0,3.838-1.706,3.838-3.8C16.4,7.904,14.678,6.2,12.562,6.2 M12.562,13C10.93,13,9.6,11.654,9.6,10c0-1.654,1.33-3,2.962-3C14.21,7,15.6,8.374,15.6,10S14.208,13,12.562,13"></path>
                    </svg>
                </span>
                <span class="tabs-info">{{__("Sécurité")}}</span> </a>
            </li>
            <li role="presentation" class="{{isset($section) && $section=="accessoire"?"active":''}}">
                <a href="{{route('show_accessoire_bien',$bien->id)}}">
                    <span class="sec-tabs-icon">
                    <svg class="svg-icon-l" viewBox="0 0 20 20">
                            <path d="M17.638,6.181h-3.844C13.581,4.273,11.963,2.786,10,2.786c-1.962,0-3.581,1.487-3.793,3.395H2.362c-0.233,0-0.424,0.191-0.424,0.424v10.184c0,0.232,0.191,0.424,0.424,0.424h15.276c0.234,0,0.425-0.191,0.425-0.424V6.605C18.062,6.372,17.872,6.181,17.638,6.181 M13.395,9.151c0.234,0,0.425,0.191,0.425,0.424S13.629,10,13.395,10c-0.232,0-0.424-0.191-0.424-0.424S13.162,9.151,13.395,9.151 M10,3.635c1.493,0,2.729,1.109,2.936,2.546H7.064C7.271,4.744,8.506,3.635,10,3.635 M6.605,9.151c0.233,0,0.424,0.191,0.424,0.424S6.838,10,6.605,10c-0.233,0-0.424-0.191-0.424-0.424S6.372,9.151,6.605,9.151 M17.214,16.365H2.786V7.029h3.395v1.347C5.687,8.552,5.332,9.021,5.332,9.575c0,0.703,0.571,1.273,1.273,1.273c0.702,0,1.273-0.57,1.273-1.273c0-0.554-0.354-1.023-0.849-1.199V7.029h5.941v1.347c-0.495,0.176-0.849,0.645-0.849,1.199c0,0.703,0.57,1.273,1.272,1.273s1.273-0.57,1.273-1.273c0-0.554-0.354-1.023-0.849-1.199V7.029h3.395V16.365z"></path>
                        </svg>
                    </span>
                    <span class="tabs-info">{{__("Accessoires")}}</span> </a>
                </li>
                <li role="presentation" class="{{isset($section) && $section=="statistique"?"active":''}}">
                        <a href="{{route('show_statistique_bien',$bien->id)}}">
                    <span class="sec-tabs-icon">
                    <svg class="svg-icon-l" viewBox="0 0 20 20">
                            <path d="M11.709,7.438H8.292c-0.234,0-0.427,0.192-0.427,0.427v8.542c0,0.234,0.192,0.427,0.427,0.427h3.417c0.233,0,0.426-0.192,0.426-0.427V7.865C12.135,7.63,11.942,7.438,11.709,7.438 M11.282,15.979H8.719V8.292h2.563V15.979zM6.156,11.709H2.74c-0.235,0-0.427,0.191-0.427,0.426v4.271c0,0.234,0.192,0.427,0.427,0.427h3.417c0.235,0,0.427-0.192,0.427-0.427v-4.271C6.583,11.9,6.391,11.709,6.156,11.709 M5.729,15.979H3.167v-3.416h2.562V15.979zM17.261,3.167h-3.417c-0.235,0-0.427,0.192-0.427,0.427v12.812c0,0.234,0.191,0.427,0.427,0.427h3.417c0.234,0,0.427-0.192,0.427-0.427V3.594C17.688,3.359,17.495,3.167,17.261,3.167 M16.833,15.979h-2.562V4.021h2.562V15.979z"></path>
                        </svg>
                    </span>
                    <span class="tabs-info">{{__("Statistiques")}}</span> </a></li-->
            </ul>
        </div>
        <div class="col-md-2">


        </div>
    </div>
</div>

@php

@endphp
