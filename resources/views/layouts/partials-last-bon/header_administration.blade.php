<nav role="navigation" class="navbar topnavbar">
    <!-- START navbar header-->
    <div class="navbar-header">
        <a href="{{url('/')}}" class="navbar-brand">
            <div class="brand-logo">
                <img src="{{asset('assets/app/kea-logo.png')}}" alt="App Logo" class="img-responsive">
            </div>
            <div class="brand-logo-collapsed">
                <img src="{{asset('assets/app/kea-logo.png')}}" alt="App Logo" class="img-responsive">
            </div>
        </a>
    </div>

    <div class="nav-wrapper">
        <ul class="nav navbar-nav navbar-left">
            <li>
                <h3 style="color: white; padding: 5px;!important; margin: 10px;">
                    Administration KEA Medicals
                </h3>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a data-toggle="tooltip" title="Aller à l'accueil" href="{{url('/')}}">
                    <span class="icon-home"></span>
                </a>
            </li>
            <li class="hidden-xs">
                <a data-toggle="tooltip" title="Notification" href="{{url('notifications')}}">
                    <span class="icon-bell">
                        @if(\Illuminate\Support\Facades\Auth::user()->nbreNotifications()> 0)
                            <i class="badge">{{\Illuminate\Support\Facades\Auth::user()->nbreNotifications()}}</i>
                        @endif
                    </span>
                </a>
            </li>
            <li class="dropdown">
                            <a href="{{ route('userProfil') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->nom." ".Auth::user()->prenom }} <span class="caret"></span>
                            </a>

                            <ul  class="dropdown-menu hidden-xs" role="menu" id='user-menu'>


                                 <li>


                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                                        <span class="fa fa-sign-out"></span> <span class="text-muted">Se déconnecter</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
        </ul>
    </div>
</nav>