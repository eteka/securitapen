<div class="appfooter">
        <div class="footer">
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-12">
                    <nav class="navbar  footnavbar">
                        <div class="container-fluid">
                            <div class="navbar-header">
                            <a class="navbar-brand light pull-left" href="{{ url('/') }}" >                           
                        <img src="{{ asset('assets/app/logo-white.png')}}"  alt="Logo">
                    </a>
                            </div>
                            <ul class="nav navbar-nav foot_menu bold">
                            <li><a href="#">A propos</a></li>
                            <li><a href="#">L'équipe</a></li>
                            <li><a href="#">Pour vous</a></li>
                            <li><a href="#">Partenaires</a></li> 
                            <li><a href="#">Corrières</a></li>
                            <li><a href="#">Blog</a></li> 
                        </ul>
                        </div>
                        </nav>
                    </div>
                    <!-- About -->
                    <div class="col-md-3 md-margin-bottom-40">
                       <h3 class="footheader">Produits</h3> 
                       <ul class="list-unstyled footlink-list">
                            <li><a href="{{ url('imu/about')}}"> Sécurisation de biens </a></li>
                             <li><a href="{{ url('blog')}}"> Sécurisation de données </a></li>
                             <li><a href="{{ url('blog')}}"> Assistance à la récupération d'objects perdus </a></li>
                            </ul> 
                    </div>
                    <div class="col-md-3 md-margin-bottom-40">
                    <h3 class="footheader">Developeurs</h3> 
                       <ul class="list-unstyled footlink-list">
                            <li><a href="{{ url('imu/about')}}"> Comment intégrer nos solutions </a></li>
                             <li><a href="{{ url('blog')}}"> La Documentation </a></li>
                             <li><a href="{{ url('blog')}}"> Forum au questions </a></li>
                            </ul> 
                    </div>
                    <div class="col-md-3 md-margin-bottom-40">
                    <h3 class="footheader">Ressources</h3> 
                       <ul class="list-unstyled footlink-list">
                            <li><a href="{{ url('imu/about')}}"> Tutoriels</a></li>
                             <li><a href="{{ url('blog')}}"> Blogs et Nouveautés </a></li>
                             <li><a href="{{ url('blog')}}"> Centre d'assistance</a></li>
                            </ul> 
                    </div>
                    <div class="col-md-3 md-margin-bottom-40">
                    <h3 class="footheader">Restons en contact</h3> 
                       <ul class="list-unstyled footlink-list">
                            <li><a href="{{ url('imu/about')}}"> <b>Email</b> info@securitapen.com</a></li>
                             <li><b>Tél</b> (229) 96 00 00 00</li>
                             <li>Cotonou, Bénin <br></li>
                            </ul> 
                    </div>
                    <!-- End Address -->
                </div>
            </div> 
        </div><!--/footer-->
</div>
<div>
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-10"> 
                        <ul class="list-inline footlink-list">
                            <li><a href="#">Conditions d'utilisations</a></li>
                            <li><a href="{{ url('imu/confidentialite') }}">Règlements de confidentialité</a></li>
                            <li><a href="">Contact</a> </li>
                            <li class="text-muted2">© Copyright, SécuritApen {{ date('Y')}} Tous droits réservés.</li>
                        </ul>                 
                        
                            
                        
                    </div>

                    <!-- Social Links -->
                    <div class="col-md-2 text-right">

                        <ul class="footer-socials list-inline text-muted2">
                            <li>
                                <a href="http://facebook.com/identitemedicale" class="tooltips text-muted2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://instagram.com/identitemedicale" class="tooltips text-muted2" data-toggle="tooltip" target="_blanck" data-placement="top" title="" data-original-title="Skype">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://myaccount.google.com/u/1/privacy" class="tooltips text-muted2" data-toggle="tooltip" target="_blanck" data-placement="top" title="" data-original-title="Google Plus">
                                    <i class="fa fa-youtube"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/id_medicale" target="_blanck" class="tooltips text-muted2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                    <!-- End Social Links -->
                </div>
            </div> 
        </div><!--/copyright-->
    </div>
    <!-- JavaScripts -->
    
    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}} -->
    <!--script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script-->
</body>
</html>
