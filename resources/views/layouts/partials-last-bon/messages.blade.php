<div class="container">
                        <div class="row">
                            <div class="col-sm-11 ">
                                @if(Session::has('flash_message')) 

                                <div class="alert padR0 mtop10 alert-success alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Information</strong> 
                                    <p>{{ Session::get('flash_message') }} </p>
                                </div>
                                @endif
                                @if(Session::has('success'))          
                                <div class="alert mtop10 alert-success alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Opération réussie</strong> 
                                    <p>{{ Session::get('success') }} </p>
                                </div>
                                @endif
                                @if(Session::has('danger'))          
                                <div class="alert mtop10 alert-danger alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Attention</strong> 
                                    <p>{{ Session::get('danger') }} </p>
                                </div>
                                @endif
                                @if(Session::has('warning'))          
                                <div class="alert mtop10 alert-warning alert-dismissible fade in" role="alert"> 
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
                                    <strong>Avertissement</strong> 
                                    <p>{{ Session::get('warning') }} </p>
                                </div>

                                @endif
                            </div>
                        </div>
                    </div>
        