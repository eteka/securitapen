<!DOCTYPE html>
<html lang="fr">
    <head>
        <title><?php
            if (isset($title)) {
                echo "" . htmlspecialchars($title) . " - Identité médicale Universelle";
            } else {
                echo "IMU | Identité médicale Universelle - KEA Software";
            }
            ?></title>
        <meta charset="utf-8">
        <link rel="SHORTCUT ICON" href="{{ asset('assets/favicon.ico') }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="identifier-url" content="" />
        <meta name="title" content=" " />
        <meta name="Description" content="L’Identité Médicale Universelle (IMU) vous permet d'écrire un profil médical qui est accessible à travers l’application mobile en cas d'urgence. Ce profil permet un accès rapide aux informations vitales telles que vos allergies, votre groupage sanguin, les personnes utiles à contacter etc…qui sont essentiels pour les premiers intervenants, les médecins ou le personnel médical qui doivent intervenir. L’application IMU a été créé pour servir les communautés médicales et les populations en offrant des cartes d'identité médicales de qualité qui permettent aux professionnels de la santé de donner un traitement rapide et précis en cas d'urgence. L’application IMU est développée à partir de la société KEA Medicals Pharmaceutics & Technologies, une société de droit Suisse basée à Genève et à but lucratif."/>
        <!-- Style CSS3 -->
        <link rel="stylesheet" href=" {{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" media="all">
        <link rel="stylesheet" href=" {{ asset('assets/plugins/bootstrap/css/bootstrap.css') }}" media="all">
        <link rel="stylesheet" href=" {{ asset('assets/css/main.css') }}" media="all">
        <!-- Javascript -->
        <script src="{{ asset('assets/plugins/jquery/jquery.min.js')}}" ></script>
        <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" ></script>
        <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" ></script>

        <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}" ></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body id="app-layout" class="bge">
        <nav class="navbar navbar-headers hpad10   navbar-static-top navbar-standard1">
            <div class="container">
                <div class="navbar-headesr col-sm-2 col-md-3 col-lg-3 pad0">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand light pull-left" href="{{ url('/') }}" >                           
                        <img src="{{ asset('assets/app/kea-logo.png')}}"  alt="KEA">

                    </a>
                    @if (!Auth::guest())
                    <!--ul class="nav navbar-nav navbar-left pad0">
                        
                    </ul-->
                    @endif
                </div>
                <div class="col-xs-8 col-sm-6  col-md-6 col-lg-6 pad5">
                    @if (!Auth::guest())
                    <div id="search-input-cover">
                        <form class="navbar-formnavbar-left " method="get" action="{{ url('search') }}" role="search">
                            <div class="input-group">  
                                <i class=" input-group-addon fa fa-search text-w no-bg"></i>
                                <input type="text" autocomplete="off" class="form-control" id="search-input" placeholder="Rechercher un hopital, un médécin, des conseils santé et bien plus">
                            </div>

                            <!--button type="submit" class="btn btn-default">Submit</button-->
                        </form>
                    </div>
                    @endif
                </div>
                <div class="collapse navbar-collapse col-xs-12 col-md-3 col-lg-3" id="app-navbar-collapse">

                    <!-- Left Side Of Navbar -->
                    <!--ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Accueil</a></li>
                    </ul-->

                    <!--ul class="nav navbar-nav navbar-right">
                      <li><a href="#">Link</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="#">Action</a></li>
                          <li><a href="#">Another action</a></li>
                          <li><a href="#">Something else here</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="#">Separated link</a></li>
                        </ul>
                      </li>
                    </ul-->
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right ">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Connexion</a></li>
                        <li><a href="{{ url('/register') }}">S'inscrire</a></li>
                        @else
                        <!--li><a href="#"><span class="fa  fa-home"></span></a></li>
                        <li><a href="#"><span class="fa  fa-star"></span></a></li>
                        <li><a href="#"><span class="fa  fa-bell-slash-o"></span></a></li-->


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ "".Auth::user()->prenom }} <span class="caret"></span>
                            </a>

                            <ul  class="dropdown-menu" role="menu" id='user-menu'>
                                <li >
                                    <div>
                                        <div class="col-xs-4">
                                            <a class="pull-left" tabindex="-1" href="{{ route('userProfil') }}"><img alt="" src="{{asset('assets/statics/users/avatar.png')}}"  class="avatar avatar-64 photo" class="img-responsive">
                                                </div>
                                                <div class="menu-wraps col-xs-8">
                                                    <ul class="list-unstyled">
                                                        <li><a href="{{ route('userProfil') }}"><span class="display-name">{{Auth::user()->nom." ".Auth::user()->prenom}}</span></a></li>

                                                    </ul>
                                                </div>
                                        </div>
                                        <li><a href="{{ url('favoris') }}"><span class="fa fa-star"></span> Favoris</a></li>
                                        <li><a href="{{ url('user/setting')}}"><span class="fa fa-cog"></span> Paramètre du compte</a></li>

                                        <li><a href="{{ url('favoris') }}"><span class="fa fa-lock"></span> Paramètres de sécurité</a></li>
                                        <li><a href="{{ url('logout')}}" class="text-muted"><span class="fa fa-sign-out"></span> Se déconnecter</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>    
        <div class="keamain">
            <div class="container">
                <div class='col-sm-12  col-sm-offset-0'>
                    @if(Session::has('flash_message'))
                    <p class="alert alert-default">
                        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                        <strong>Parfait !</strong><br>
                        {!! Session::get('flash_message') !!} 
                    </p>
                    @endif
                    @if(Session::has('info'))
                    <p class="alert alert-info">
                        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                        <strong>Information !</strong><br>
                        {!! Session::get('info') !!}
                    </p>
                    @endif
                    @if(Session::has('danger'))
                    <p class="alert alert-danger">
                        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                        <strong>Désolé !</strong><br>
                        {!! Session::get('danger') !!}
                    </p>
                    @endif
                    @if(Session::has('success'))
                    <p class="alert alert-success">
                        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                        <strong>Bravo !</strong><br>
                        {!! Session::get('success') !!}
                    </p>
                    @endif
                    @if(Session::has('warning'))
                    <p class="alert alert-warning">
                        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                        <strong>Attention !</strong><br>
                        {!! Session::get('warning') !!}
                    </p>
                    @endif
                </div>
            </div>
            @yield('content')  
        </div>
        @include ('layouts.footer')
    </body>
</html>
