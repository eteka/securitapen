

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from themicon.co/theme/angle/v3.5.3/backend-jquery/app/login.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 06 Jan 2017 11:21:31 GMT -->
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App + jQuery">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <title>@yield('title',"Admin")</title>
   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="{{asset('assets/vendor/fontawesome/css/font-awesome.min.css')}}">
   <!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="{{asset('assets/vendor/simple-line-icons/css/simple-line-icons.css')}}">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="{{asset('assets/app/css/bootstrap.css')}}" >
   <!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="{{asset('assets/app/css/app.css')}}" id="maincss">
</head>

<body>


    @yield('content')

    @yield('srcipt')
   <!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <!--script src="../vendor/modernizr/modernizr.custom.js"></script>
   <!-- JQUERY->
   <script src="../vendor/jquery/dist/jquery.js"></script>
   <!-- BOOTSTRAP->
   <script src="../vendor/bootstrap/dist/js/bootstrap.js"></script>
   <!-- STORAGE API->
   <script src="../vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
   <!-- PARSLEY->
   <script src="../vendor/parsleyjs/dist/parsley.min.js"></script>
   <!-- =============== APP SCRIPTS ===============->
   <script src="js/app.js"></script-->
</body>
</html>
