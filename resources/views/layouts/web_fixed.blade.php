<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>@yield('title','IMU | Identité médicale Universelle - KEA Software')</title>
        <meta charset="utf-8">
        <link rel="SHORTCUT ICON" href="{{ asset('assets/favicon.ico') }}" />
        <link rel="shortcut icon" href="{{ asset('assets/favicon.ico') }}">
        <link rel="apple-touch-icon" href="{{ asset('images/apple-touch-icon.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/apple-touch-icon72x72.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/apple-touch-icon114x114.png') }}">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="identifier-url" content="https://keamedical.com" />
        <meta name="title" content="KEA Medical - Programme IMU" />
        <meta name="Description" content="L’Identité Médicale Universelle (IMU) vous permet d'écrire un profil médical qui est accessible à travers l’application mobile en cas d'urgence. Ce profil permet un accès rapide aux informations vitales telles que vos allergies, votre groupage sanguin, les personnes utiles à contacter etc…qui sont essentiels pour les premiers intervenants, les médecins ou le personnel médical qui doivent intervenir. L’application IMU a été créé pour servir les communautés médicales et les populations en offrant des cartes d'identité médicales de qualité qui permettent aux professionnels de la santé de donner un traitement rapide et précis en cas d'urgence. L’application IMU est développée à partir de la société KEA Medicals Pharmaceutics & Technologies, une société de droit Suisse basée à Genève et à but lucratif."/>
        <!-- Style CSS3 -->
        <link rel="stylesheet" href=" {{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" media="all">
        <link rel="stylesheet" href=" {{ asset('assets/plugins/bootstrap/css/bootstrap.css') }}" media="all">
        <link rel="stylesheet" href=" {{ asset('assets/plugins/line-icons/css/simple-line-icons.css') }}" media="all">
        <!--<script src="{{asset('assets/js/jquery.min.js')}}"></script>-->
        <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/plugins/pjax/jquery.pjax.js')}}"></script>


        <link rel="stylesheet" href=" {{ asset('assets/css/main.css') }}" media="all">
        @yield('css')
    </head>
    <body id="app-layout" class="bgse">
        <div id="loading">
            <div id="iot-preloader">
                <div class="center-preloader d-flex align-items-center">
                    <div class="spinners">
                        <div class="spinner01"></div>
                        <div class="spinner02"></div>
                    </div>
                </div>
            </div>

    <!--<img src='http://www.edukily.com/img/loading.gif' alt="Chargement..." class="img-circle" height="30px ">-->
        </div>
        <style>
            /*
            https://www.icao.int/training/images/loading.gif
            http://www.edukily.com/img/loading.gif
            https://cdn.dribbble.com/users/154550/screenshots/3014408/untitled-9.gif
            */
            #iot-preloader, .card-preloader {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                width: 100%;
                height: 100%;
                /*background-color: rgba(22,30,37,.9); rgba(244,245,253,.8)*/
                background: rgba(244,245,253,.8);
                z-index: 1040;
                display: block;
                overflow: hidden;                
                box-shadow: inset 0 0 4rem rgba(15,156,230,.45);
            }
            #iot-preloader .center-preloader, .card-preloader .center-preloader {
                height: 100%;
                vertical-align: middle;
            }
            .align-items-center {
                -webkit-box-align: center !important;
                -webkit-align-items: center !important;
                -ms-flex-align: center !important;
                align-items: center !important;
            }
            .d-flex {
                display: -webkit-box !important;
                display: -webkit-flex !important;
                display: -ms-flexbox !important;
                display: flex !important;
            }
            body {
                font-family: Poppins,-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;

            }

            #iot-preloader .center-preloader .spinners, .card-preloader .center-preloader .spinners {
                width: 60px;
                height: 60px;
                margin: 0 auto;
            }
            #iot-preloader .center-preloader .spinners .spinner01, 
            .card-preloader .center-preloader .spinners .spinner01 {
                border: 5px solid rgba(15,156,230,.9);
                border-right-width: 5px;
                border-left-width: 5px;
                border-right-style: solid;
                border-left-style: solid;
                border-right-color: rgba(15, 156, 230, 0.9);
                border-left-color: rgba(15, 156, 230, 0.9);
                opacity: .9;
                border-right: 5px solid transparent;
                border-left: 5px solid transparent;
                border-radius: 50%;
                box-shadow: 0 0 35px rgba(15,156,230,.45);
                width: 50px;
                height: 50px;
                margin: 0 auto;
                will-change: transform,opacity;
                -webkit-animation: spinPulse 1s infinite ease-in-out;
                animation: spinPulse 1s infinite ease-in-out;
            }
            #iot-preloader .center-preloader .spinners .spinner02,
            .card-preloader .center-preloader .spinners .spinner02 {
                border: 5px solid rgba(15,156,230,.9);
                border-right-width: 5px;
                border-left-width: 5px;
                border-right-style: solid;
                border-left-style: solid;
                border-right-color: rgba(15, 156, 230, 0.9);
                border-left-color: rgba(15, 156, 230, 0.9);
                opacity: .9;
                border-left: 5px solid transparent;
                border-right: 5px solid transparent;
                border-radius: 50%;
                box-shadow: 0 0 15px rgba(15,156,230,.45);
                width: 30px;
                height: 30px;
                margin: 0px auto ;
                position: relative;
                top: -40px;
                will-change: transform;
                -webkit-animation: spinoffPulse 1s infinite linear;
                animation: spinoffPulse 1s infinite linear;
            }
            @-webkit-keyframes iconPulse {
                0%,100% {
                    -webkit-transform:scale(1);
                    transform:scale(1);
                    opacity:1
                }
                50% {
                    -webkit-transform:scale(1.3);
                    transform:scale(1.3);
                    opacity:.75
                }
            }
            @keyframes iconPulse {
                0%,100% {
                    -webkit-transform:scale(1);
                    transform:scale(1);
                    opacity:1
                }
                50% {
                    -webkit-transform:scale(1.3);
                    transform:scale(1.3);
                    opacity:.75
                }
            }
            @-webkit-keyframes spinPulse {
                0% {
                    -webkit-transform:rotate(160deg);
                    transform:rotate(160deg);
                    opacity:0
                }
                50% {
                    -webkit-transform:rotate(145deg);
                    transform:rotate(145deg);
                    opacity:1
                }
                100% {
                    -webkit-transform:rotate(-320deg);
                    transform:rotate(-320deg);
                    opacity:0
                }
            }
            @keyframes spinPulse {
                0% {
                    -webkit-transform:rotate(160deg);
                    transform:rotate(160deg);
                    opacity:0
                }
                50% {
                    -webkit-transform:rotate(145deg);
                    transform:rotate(145deg);
                    opacity:1
                }
                100% {
                    -webkit-transform:rotate(-320deg);
                    transform:rotate(-320deg);
                    opacity:0
                }
            }
            @-webkit-keyframes spinoffPulse {
                0% {
                    -webkit-transform:rotate(0);
                    transform:rotate(0)
                }
                100% {
                    -webkit-transform:rotate(360deg);
                    transform:rotate(360deg)
                }
            }
            @keyframes spinoffPulse {
                0% {
                    -webkit-transform:rotate(0);
                    transform:rotate(0)
                }
                100% {
                    -webkit-transform:rotate(360deg);
                    transform:rotate(360deg)
                }
            }

            #loading {
                position:fixed;
                width:20%;
                /*height:35px;*/
                padding: 12px;
                right:0%;
                text-align: center;
                /*background: rgba(0,0,0,.58);*/
                z-index: 5000;
                /*display:none;*/
            }
        </style>
        <nav class="navbar navbar-headers hpad10   navbar-fixed-top navbar-standard1">
            <div class="container">
                <div class="navbar-headesr col-sm-2 col-md-2 col-lg-3 pad0">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-togglecollapsed hidden" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    @if (!Auth::guest())

                    <ul class="list-unstyled list-inline text-white hidden visible-xs xs-menu arial">


                        <li><a href="{{ route('userProfil') }}"><i class="fa fa-user"></i>  {{ Auth::user()->prenom }}</a></li>
                        <li>|</li>
                        <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="hidden">Accueil</span></a></li>
                        <li>|</li>
                        <li>
                            <a href="{{ url('/') }}">
                                <i class="fa fa-bell-o"></i> <span class="hidden">Notification</span></a>
                        </li>


                        <li>|</li>  
                        <li><a href="{{ url('/') }}"><i class="fa fa-sign-out"></i></a></li>
                    </ul>
                    <!--ul class="nav navbar-nav navbar-left pad0">
                        
                    </ul-->
                    @endif
                    <!-- Branding Image -->
                    <a class="navbar-brand light pull-left" href="{{ url('/') }}" >                           
                        <img src="{{ asset('assets/app/kea-logo.png')}}"  alt="KEA">
                    </a>
                    <div id="langdiv">
                        <div id="google_translate_element"></div>
                    </div>                 
                </div>

                @if (!Auth::guest())
                <div class="col-xs-12 col-sm-8   col-md-6 col-lg-6 ">
                    <div id="search-input-cover">

                        <style>
                            /* nav bar search box - drop down menu button */
                            .navbar .navbar-search .dropdown-menu { min-width: 25px; }
                            .dropdown-menu .label-icon { margin-left: 5px; }
                            .btn-outline {
                                background-color: transparent;
                                color: inherit;
                                transition: all .5s;
                            }

                        </style>

                        <form class="navbar-formnavbar-left " method="GET" action="{{ url('search') }}"  role="search">
                            <div class="form-horizontal" role="form">

                            </div>
                            <div class="input-group hiddenss" id="adv-search">

                                <input type="text" class="form-control input-sm" name="query" value="{{isset($_GET['query'])?$_GET['query']:NULL}}" id="search-input" placeholder="Rechercher un hopital, un médécin, des conseils santé et bien plus" />
                                <div class="input-group-btn mtop10">
                                    <div class="btn-group" role="group">
                                        <div class="dropdown dropdown-lg">
                                            <button type="button" class="btn btn-default input-sm dropdown-toggle" id="setting-search-btn" data-toggle="dropdown" aria-expanded="false"><span class=" fa fa-cog  no-bg"></span></button>
                                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                                                <div class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label for="filter">Filter by</label>
                                                        <select name="type" class="form-control">
                                                            <option value="0" selected>Toutes les options</option>
                                                            <option value="1">Médecins</option>
                                                            <option value="2">Pharmacies</option>
                                                            <option value="3">Salle de Gyme</option>
                                                            <option value="4">Banque de Sang</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="contain">Pays</label>
                                                        <select name="country" class="form-control">
                                                            <option value="0" selected>Bénin</option>
                                                            <option value="1">Togo</option>
                                                            <option value="2">Niger</option>
                                                            <option value="3">Mali</option>
                                                            <option value="4">Nigéria</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="contain">Localité</label>
                                                        <input name="localtion" type="text" class="form-control">
                                                    </div>
                                                    <button type="submit"  class="btn btn-primary search_btn rond3 btn-block"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Rechercher</button>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button"  id="search-add-btn" class="no-bg btn btn-primary input-sm bold"><span class="text-whit glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                    </div>
                                </div>
                            </div>
                        </form>                        
                    </div>
                </div>
                @else
                <div class="col-xs-12 col-sm-6  col-md-7 col-lg-6 ">

                </div>
                @endif

                <div class="collapse navbar-collapse   col-xs-12 col-sm-4 col-md-3 col-lg-3" id="app-navbar-collapse">

                    <!-- Right Side Of Navbar -->
                    <ul id="Rnavbar" class="nav navbar-nav navbar-right ">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Connexion</a></li>
                        <li><a href="{{ url('/register') }}">S'inscrire</a></li>
                        @else

                        <li><a data-toggle="tooltip" title="Aller à l'accueil" href="{{url('/')}}"><span class="icon-home"></span></a></li>

                        <li><a data-toggle="tooltip" title="Carnet Médical" href="{{url('carnet')}}"><span class="icon-notebook"></span></a></li>

                        <li><a data-toggle="tooltip" title="Notification" href="{{url('notifications')}}"><span class="icon-bell"></span></a></li>
                        <li><a data-toggle="tooltip" title="Mes rendez-vous" href="{{url('carnet')}}"><span class="icon-clock"></span></a></li>


                        <li class="dropdown">
                            <a href="{{ route('userProfil') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ "".Auth::user()->prenom }} <span class="caret"></span>
                            </a>

                            <ul  class="dropdown-menu hidden-xs" role="menu" id='user-menu'>
                                <li >
                                    <div class="clearfix pad10">
                                        <div class="col-xs-12 text-center ">
                                            <a class="" tabindex="-1" href="{{ url('profil')}}">
                                                <img alt="User" style=""  src="{{asset('assets/statics/users/avatar.png')}}"  class="avatar avatar-user shadow photo" >
                                            </a>
                                        </div>
                                        <div class="menu-wraps col-xs-12">
                                            <h3 class="display-name">
                                                <a href="{{ url('profil',Auth::user()->username) }}"><span >{{Auth::user()->nom." ".Auth::user()->prenom}}</span></a>
                                            </h3>
                                            <div class=" text-xs text-center pad5_0">
                                                <a class="text-info" href="#"><span class="fa fa-pencil"></span> Editer le profil</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="pad5">
                                        <!--                                        <ul class="list-unstyled pad0s">
                                                                                    <li role="separator" class="divider"></li>
                                                                                </ul>-->
                                    </div>
                                </li>

                                <li class="dropdown-header text-uppercase">Mon compte</li>
                                <li>
                                    <a href="{{ route('AccountProfil',Auth::user()->username) }}"><span class="icon-user"></span> Voir mon profil</a>
                                </li>
                                <li><a href="{{ url('user/setting')}}"><span class="icon-equalizer"></span> Paramètre du compte</a></li>
                                <li>
                                    <a href="#"><span class="icon-list"></span> Activités</a>
                                </li>
                                <li class="dropdown-header  text-uppercase">Aide et langues</li>
                                <li>
                                    <a href="#"><span class="icon-support"></span> Centre d'assistance</a>
                                </li>  
                                <li>
                                    <a href="#"><span class="icon-globe"></span> Langues</a>
                                </li>
                                <li role="separator" class="divider"></li>                             <!--li><a href="{{ url('services/qr-code/')}}"><span class="fa fa-qrcode"></span> Mode code </a></li-->
                                <li><a href="{{ url('logout')}}" ><span class="fa fa-sign-out"></span> <span class="text-muted">Se déconnecter</span></a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @if (!Auth::guest() && !isset($nohead) && 0)

        <div class="line-std2">
            <div class="container">
                <div class="row ">
                    <div class="col-sm-8  col-xs-12 col-md-6 col-xs-offset-0 col-sm-offset-2 col-md-offset-3 text-center text-center ">
                        <ul id="head-tabs" class="nav nav-tabs">
                            <li class="{{ (isset($page) && $page=='carnet')? 'active':'' }}"><a href="{{ url('/carnet') }}"><span class="fa fa-book"></span> <span class="hidden-xs">Carnet de soins</span></a></li>
                            <li class="{{ (isset($page) && $page=='traitements')? 'active':'' }}"><a href="{{ url('/traitements') }}"><span class="fa fa-medkit"></span> <span class="hidden-xs">Traitements</span></a></li>
                            <li class="{{ (isset($page) && $page=='rdv')? 'active':'' }}"><a href="{{ url('/rdv') }}"><span class="fa  fa-history"></span> <span class="hidden-xs">Rendez-vous</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div id="keamain" class="keamain">
            @if(Session::has('info') || Session::has('danger') || Session::has('info')  || Session::has('success') || Session::has('warning'))
            <div class='container '>
                <div class='row'>
                    <div class='col-sm-8 col-sm-offset-2'>
                        @if(Session::has('info'))
                        <p class="message mtop10 message-info"><i class="fa fa-info-circle"></i> {!! Session::get('info') !!}</p>
                        @endif
                        @if(Session::has('danger'))
                        <p class="message mtop10 message-danger"><i class="fa fa-times-circle"></i> {!! Session::get('danger') !!}</p>
                        @endif
                        @if(Session::has('warning'))
                        <p class="message mtop10 message-warning">
                            <i class="fa fa-warning"></i> {!! Session::get('warning') !!}
                        </p>
                        @endif
                        @if(Session::has('success'))
                        <p class="message mtop10 message-success"> <i class="fa fa-check"></i> {!! Session::get('success') !!}</p>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            @yield('content')  
             <!--<script src="javascripts/jquery.1.8.2.min.js"></script>-->
            <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
            <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" ></script>
            <script>
            $(function () {
                $('#loading,#iot-preloader').fadeOut('slow');
                $('[data-toggle="tooltip"]').tooltip();
            })
            </script>
            
           <!--<script type="text/javascript" src="{{asset('assets/plugins/djax/jquery.djax.js')}}"></script>
           <script type="text/javascript" src="{{asset('assets/plugins/djax/example.js')}}"></script>-->
            <!--<script type="text/javascript" src="{{asset('assets/plugins/nprogress/nprogress.js')}}"></script>-->
            <!--<link rel="stylesheet" href=" {{ asset('assets/plugins/nprogress/nprogress.css') }}" media="all">-->
            <!-- Javascript -->

            <!--<script src="{{ asset('assets/plugins/mydatepicker/js/bootstrap-datepicker.js') }}" ></script>-->
            <!--<link rel="stylesheet" href=" {{ asset('assets/plugins/mydatepicker/css/datepicker.css') }}" media="all">-->
            <!--<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}" ></script>-->

            <!--<link href="{{ asset('assets/plugins/myselect2/select2.min.css')}}" rel="stylesheet" />-->
            <!--<script src="{{ asset('assets/plugins/myselect2/select2.min.js')}}"></script>-->
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            
            <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
            @yield('script')
            @yield('scripts')
            <script src="{{asset('assets/plugins/navigate/navigate.min.js')}}"></script> 
            <script>
$(document).ready(function () {
//    

});
            </script>
        </div>

        @include ('layouts.footer')

        <link rel="stylesheet" href="{{asset('assets/plugins/pace/themes/white/pace-theme-minimal.css')}}" />

        <script>
            paceOptions = {
                elements: true
            };
        </script>
        <script src="{{asset('assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js')}}"></script>
        <script src="{{asset('assets/plugins/pace/pace.min.js')}}"></script>
        <script>


            //    setTimeout(function(){
            //      Pace.ignore(function(){
            //        
            //      });
            //    }, 4000);

            //    Pace.on('hide', function(){
            //      console.log('done');
            //    });
        </script>
        <!--<input id="range" type="range" min="0" max="100">-->
      <!--  <script>
          range.addEventListener('input', function(){
            document.querySelector('.pace').classList.remove('pace-inactive');
            document.querySelector('.pace').classList.add('pace-active');
      
            document.querySelector('.pace-progress').setAttribute('data-progress-text', range.value + '%');
            document.querySelector('.pace-progress').setAttribute('style', '-webkit-transform: translate3d(' + range.value + '%, 0px, 0px)');
          });
        </script>-->

    </body>
</html>
