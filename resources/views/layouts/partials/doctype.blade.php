<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>
          @section('title')
          @if(isset($title) && $title!=NULL){{$title}} -  SécuritApen @else  {{__("SécuritApen, pour plus de sécurité")}}
          @endif

          @show
        </title>
          <meta charset="utf-8">
        <link rel="SHORTCUT ICON" href="{{ asset('assets/favicon.ico') }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="identifier-url" content="{{Request::fullUrl()}}" />
        <meta name="theme-color" content="#244157"/>
        <link rel="icon" sizes="192x192" href="{{asset("assets/statics/app/logo-full-carre.jpg")}}">
        <!--meta name="theme-color" content="#10CF7F"/-->
        <meta name="Description" content="Sécuritapen est une application qui vous permet d'enrégistrer des informations sur vos biens et données afin de vous aider à les retrouver en cas de perte"/>
        <!-- Style CSS3 -->
        <!--link rel="stylesheet" href=" {{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" media="all"-->
        <link rel="stylesheet" href=" {{ asset('assets/plugins/bootstrap/css/bootstrap.css') }}" media="all">
        <script type="text/javascript" src="{{ asset('assets/plugins/turbolinks/turbolinks.js')}}"></script>


        <!--link rel="stylesheet" href="{{ asset('assets/plugins/line-icons/css/simple-line-icons.css') }}"-->
        <!-- Javascript -->
        <!--script src="{{ asset('assets/plugins/jquery/jquery.min.js')}}" ></script>
        <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" ></script>
        <script src="{{ asset('assets/plugins/mydatepicker/js/bootstrap-datepicker.js') }}" ></script>
        <link rel="stylesheet" href=" {{ asset('assets/plugins/mydatepicker/css/datepicker.css') }}" media="all">
        <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}" ></script>

        <link href="{{ asset('assets/plugins/myselect2/select2.min.css')}}" rel="stylesheet" />
        <script src="{{ asset('assets/plugins/myselect2/select2.min.js')}}"></script-->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.j"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href=" {{ asset('assets/css/main.css') }}" media="all">
        @yield('css')
    </head>
