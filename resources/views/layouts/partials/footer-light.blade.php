
<div>
        <div class="copyright-light bgwhite">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="list-inline text-uppercase bold text-sm footlink-lists">
                            <li><a href="{{route('condition')}}">{{__("Conditions d'utilisations")}}</a></li>
                            <li><a href="{{route('reglement')}}">{{__("Règlements de confidentialité")}}</a></li>
                            <li><a href="{{route('soumission')}}">{{__("Participer")}}</a> </li>
                        <li class="hidden-xs">&nbsp;&nbsp;|&nbsp;&nbsp;</li>
                        <li>
                            {{__('Langue :')}}


                            <a href="{{url('lang/fr')}}">{{__("Français")}}</a>

                            /

                                <a href="{{url('lang/en')}}">{{__("Anglais")}}</a>

                        </li>
                        <li>


                            <small class="text-muted" >
                                © SécuritApen 2020. {{__("Tous droits réservés")}}

                        </li>
                        </ul>

                        </div>
                    </div>

                    <!-- Social Links -->
                    <!--div class="col-md-2 text-right">

                        <ul class=" footer-socials list-inline">
                            <li>
                                <a href="http://facebook.com/identitemedicale" class="tooltips text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://instagram.com/identitemedicale" class="tooltips text-primary" data-toggle="tooltip" target="_blanck" data-placement="top" title="" data-original-title="Skype">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://myaccount.google.com/u/1/privacy" class="tooltips text-primary" data-toggle="tooltip" target="_blanck" data-placement="top" title="" data-original-title="Google Plus">
                                    <i class="fa fa-youtube"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/id_medicale" target="_blanck" class="tooltips text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>

                        </ul>
                    </div-->

                    <!-- End Social Links -->

                </div>
            </div>
        </div><!--/copyright-->
    </div>
    <!-- JavaScripts -->

    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
     {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}-->
     <!--script type="text/javascript">

function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script-->
</body>
</html>
