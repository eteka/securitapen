<nav class="navbar navbar-headers  navbar-static-top navbar-default nav-home">
            <div class="container pad0-xs">
                <div class="navbar-headesr col-sm-2 col-md-2 col-lg-3 pad0">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-togglecollapsed hidden" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    @if (!Auth::guest())

                    <ul class="list-unstyled list-inline text-white hidden visible-xs xs-menu arial">


                        <li><a href="{{ url('userProfil') }}"><i class="fa fa-user"></i>  {{ Auth::user()->prenom }}</a></li>
                        <li>|</li>
                        <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span class="hidden">Accueil</span></a></li>
                        <li>|</li>
                        <li><a href="{{ url('/') }}"><i class="fa fa-bell-o"></i> <span class="hidden">Notification</span></a></li>


                        <li>|</li>
                        <li><a href="{{ url('/') }}"><i class="fa fa-sign-out"></i></a></li>
                    </ul>
                    <!--ul class="nav navbar-nav navbar-left pad0">

                    </ul-->
                    @endif
                    <!-- Branding Image -->
                    <a class="navbar-brand light pull-left" href="{{ url('/') }}" >
                        <img src="{{ asset('assets/app/logo-v1.png')}}" class="hidden-xs hidden-sm"  alt="{{config('app.name')}}">
                        <img src="{{ asset('assets/statics/app/logo-xs-sec.png')}}" class="visible-xs visible-sm"  alt="{{config('app.name')}}">

                    </a>
                    <!--div id="langdiv">
                        <div id="google_translate_element"></div>
                    </div-->
                </div>

                @if (!Auth::guest())
                <div class="col-xs-12 col-sm-8   col-md-6 col-lg-6 ">
                    <div id="search-input-cover">
                        <form class="navbar-formnavbar-left " method="get" action="{{ url('search') }}" role="search">
                            <div class="input-group">
                                <i class=" input-group-addon fa fa-search text-w no-bg"></i>
                                <input type="text" autocomplete="off" class="form-control input-sm bgsec1 borderd" id="search-input" placeholder="Rechercher ...">
                            </div>

                            <!--button type="submit" class="btn btn-default">Submit</button-->
                        </form>
                    </div>
                </div>
                @else
                <div class="col-xs-12 col-sm-8 hidden-xs col-md-7 col-lg-7">

                </div>
                @endif

                <div class=" navbar-collapse   col-xs-8 text-xs-right col-sm-4 col-md-3 col-lg-3" id="app-navbar-collapse">

                    <!-- Left Side Of Navbar -->
                    <!--ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Accueil</a></li>
                    </ul-->

                    <!--ul class="nav navbar-nav navbar-right">
                      <li><a href="#">Link</a></li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="#">Action</a></li>
                          <li><a href="#">Another action</a></li>
                          <li><a href="#">Something else here</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="#">Separated link</a></li>
                        </ul>
                      </li>
                    </ul-->
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right ">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        <li><a class="bold" class="borderc " href="{{ url('/login') }}"> @lang("Connexion")</a></li>
                        <li class="hidden"><a class="bold" href="{{ url('/register') }}"> @lang("S'inscrire")</a></li>
                        @else
                        <!--li><a href="#"><span class="fa  fa-home"></span></a></li>
                        <li><a href="#"><span class="fa  fa-star"></span></a></li-->
                        <li><a href="#"><span class="fa  fa-bell-o"></span></a></li>


                        <li class="dropdown">
                            <a href="{{ url('userProfil') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ "".Auth::user()->prenom }} <span class="caret"></span>
                            </a>

                            <ul  class="dropdown-menu hidden-xs" role="menu" id='user-menu'>
                                <li >
                                    <div>
                                        <div class="col-xs-4 ">
                                            <a class="pull-left" tabindex="-1" href="{{ url('profil')}}"><img alt="" src="{{asset('assets/statics/users/avatar.png')}}"  class="avatar avatar-64 photo" class="img-responsive">
                                                </div>
                                                <div class="menu-wraps col-xs-8">
                                                    <ul class="list-unstyled">
                                                        <li><a href="{{ url('profil',Auth::user()->username) }}"><span class="display-name">{{Auth::user()->nom." ".Auth::user()->prenom}}</span></a></li>
                                                        <li><a href="#"><span class="fa fa-edit"></span> Modifier</a></li>
                                                    </ul>
                                                </div>
                                        </div>
                                </li>
                                <li><a href="{{ url('user/setting')}}"><span class="fa fa-cog"></span> Paramètre du compte</a></li>
                                                                <!--li><a href="{{ url('services/qr-code/')}}"><span class="fa fa-qrcode"></span> Mode code </a></li-->
                                <li><a href="{{ url('logout')}}" class="text-muted"><span class="fa fa-sign-out"></span> Se déconnecter</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                @section('notification')
                {{srand(100)}}
                @show
                </div>
            </div>
        </div>
        <!--ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
        <a class="nav-link" href="#" id="navbarDropdownFlag" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <img width="32" height="32" alt="{{ session('locale') }}"
                    src="{!! asset('images/flags/' . session('locale') . '-flag.png') !!}"/>
        </a>
        @if(is_array(config('app.locale')))
        <div id="flags" class="dropdown-menu" aria-labelledby="navbarDropdownFlag">
            @foreach(config('app.locale') as $locale)
                @if($locale != session('locale'))
                    <a class="dropdown-item" href="{{ route('language', $locale) }}">
                        <img width="32" height="32" alt="{{ session('locale') }}"
                                src="{!! asset('images/flags/' . $locale . '-flag.png') !!}"/>
                    </a>
                @endif
            @endforeach
        </div>
        @endif
    </li>
    </ul-->
