
<html lang="en">
<head>
    <title>Laravel DataTables Tutorial Example</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
            
    <link  href="{{asset('assets/plugins/datatable/jquery.dataTables.min.css')}}" rel="stylesheet">
    

    <script src="{{ asset('assets/plugins/jquery/2.1.3/jquery.min.js')}}"></script>  
        <script src="{{ asset('assets/plugins/datatable/jquery.dataTables.min.js')}}"></script>
        <script src="{{ asset('assets/plugins/datatable/dataTables.bootstrap.js')}}"></script>
        <script src="{{ asset('assets/plugins/datatable/dataTables.responsive.js')}}"></script>
        
</head>
      <body>
         <div class="container">
               <h2>Laravel DataTables Tutorial Example</h2>
            <table class="table table-bordered" id="table">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Name</th>
                     <th>Email</th>
                  </tr>
               </thead>
            </table>
         </div>
       <script>
         $(function() {
               $('#table').DataTable({
               processing: true,
               serverSide: true,
               ajax: '{{ route('apiv1GetUser') }}',
               columns: [
                        { data: 'id', name: 'id' },
                        { data: 'name', name: 'name' },
                        { data: 'email', name: 'email' }
                     ]
            });
         });
         </script>
   </body>
</html>