@extends('layouts.app')
@section('title')
{{$count_bien.' '.$s_count_bien}} -
@parent
@endsection
@section('nav')
@include('layouts.partials.topnav-dark',['tabs'=>"home"])
@include('apps.includes.nav.notification')
@endsection
@section('footer')

@endsection
@push('script')
{!!jsPlugin('jscroll')!!}

<script type="text/javascript">

$(function() {
  $('.main-section').jscroll({
    autoTrigger: true,
    loadingHtml: '<div id="cloader" class="cloader  sec_card"><div class="loader"></div></div>',
    padding: 0,
    nextSelector: '.pagination li.active + li a',
    contentSelector: 'div.main-section>div',
    callback: function() {
      $('ul.pagination').remove();
      $('ul.pagination').hide();
      //alert('OK')
    }
  });
});

</script>
{!!jsPlugin('sticky')!!}
<script>
$(document).ready(function(){
  //$(".navbar-standard2").css('ZIndex',555555).sticky({topSpacing:0});
  $("#right,#profil-stick").sticky({topSpacing: 0});
});
</script>
@endpush

@section('content')
<div class="main">
  <div class="container">
    <div class="row">
      <div class="col-md-2 hidden-xs" id="left">
        <div id="profil-stick">
          @include ('apps.includes.nav.user-profil')
        </div>
      </div>
      <div class="col-md-7 ">
        <div class="top_home_param_bloc ">
          <div class="row">
            <div class="col-xs-12 text-center-xs col-sm-5 col-lg-6">
              @if(isset($count_bien) && $count_bien!=NULL)
              <span class=" text-muted bold"><i class="icon-organization"></i> <a class="text-sm text-muted bold nodecoration" href="{{route('classement')}}">{{$count_bien}}</a></span><span class="scarect scaret_r text-sm  bold text-muted">  {{isset($s_count_bien)?$s_count_bien:"-"}} ({{ isset($nb)? $nb:'0'}})</span>
              @endif
            </div>
            <div class="col-xs-12 text-center-xs mtop-xs-10 col-sm-7 col-lg-6 text-right">

              {!! Form::open(['route' => ['classement_mode',isset($slug)?$slug:'-'], 'class' => 'form-horizontal', 'method' => 'get']) !!}
              <div class="top-line-options">
                <span class="hidden-xs"><a title="{{__('Tableau de bord')}}" href="{{route('dashboard')}}" class="btn btn-md btn-default"><i class="icon-home"></i></a></span>

                <span><a href="{{route('bien_favoris')}}" class="btn btn-md btn-default"><i class="icon-star"></i></a></span>
                <span><a href="{{route('classement')}}" title="{{__('Classement')}}" class="btn btn-md bold "><i class="icon-organization"></i></a></span>


                <a title="{{__('Exporter ce mode')}}" href="{{route('export_mode',isset($slug)?$slug:'-')}}" class="btn btn-md btn-default">
                <svg class="svg-icon svg-icon-md rotate180" viewBox="0 0 20 20">
                      <path fill="none" d="M8.416,3.943l1.12-1.12v9.031c0,0.257,0.208,0.464,0.464,0.464c0.256,0,0.464-0.207,0.464-0.464V2.823l1.12,1.12c0.182,0.182,0.476,0.182,0.656,0c0.182-0.181,0.182-0.475,0-0.656l-1.744-1.745c-0.018-0.081-0.048-0.16-0.112-0.224C10.279,1.214,10.137,1.177,10,1.194c-0.137-0.017-0.279,0.02-0.384,0.125C9.551,1.384,9.518,1.465,9.499,1.548L7.76,3.288c-0.182,0.181-0.182,0.475,0,0.656C7.941,4.125,8.234,4.125,8.416,3.943z M15.569,6.286h-2.32v0.928h2.32c0.512,0,0.928,0.416,0.928,0.928v8.817c0,0.513-0.416,0.929-0.928,0.929H4.432c-0.513,0-0.928-0.416-0.928-0.929V8.142c0-0.513,0.416-0.928,0.928-0.928h2.32V6.286h-2.32c-1.025,0-1.856,0.831-1.856,1.856v8.817c0,1.025,0.832,1.856,1.856,1.856h11.138c1.024,0,1.855-0.831,1.855-1.856V8.142C17.425,7.117,16.594,6.286,15.569,6.286z"></path>
                    </svg>
                  </a>
                </span>

                {!! Form::select('orderby',["asc"=>__("Plus récent"),"desc"=>__("Plus ancien")], app('request')->input('orderby'),['class' => 'btn-default text-md btn l-inline form-control-md', 'required' => 'required',"onchange"=>"this.form.submit()"]) !!}
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
        <div class="main-section">
          @isset($biens)
          @include('apps.includes.data.liste-biens',['biens'=>$biens,"icone"=>"icon-organization","msg"=>__('Aucun élément dans le classement')])
          @endif
        </div>
      </div>

      <div   class="col-md-3 hidden-xs  minh">
        <div id="right">
          <div  class="mtop15 text-sm text-muted mbottom10">
            <span class="icon-tag "></span> Des Boutiques à découvrir
          </div>
          <div class=" sec_cardbgwhiteshadow3noborderrond5">

            <div>
              <img class="img-responsive rond5" src="{{asset('storage/statics/pub/affiche-1.jpg')}}" alt="Publicité pate dentifrice">
            </div>
          </div>
          @include('layouts.partials.mini-footer')
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
