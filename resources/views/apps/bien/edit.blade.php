@extends('layouts.app')
@section('title',__('Edition bien'))
@section('nav')
        @include('layouts.partials.topnav-dark')
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card mbottom40 mtop15 sec_card">
                <div class="sec_bg-info">
                       <div class="card-body">
                            <h3 class="Subhead-heading ">{{__("Edition de bien")}}</h3>
                            
                            <p class="Subhead-description text-sm text-muted">
                            {{__("Il s'agit de tout bien, qu'il soit matériel physique ou non que vous venez d'acquérir ou que vous avez acheté dans le passé et qui est en votre nom. 
                            Avez-vous des difficultés ?")}} , <a href="#">{{__("touvez de l'aide")}}.</a>
                            </p>
                        </div>
                        </div>
                    <div class="card-body">
                        <a href="{{ url('/') }}" title="Back"><button class="btn btn-link btn-md"><i class="glyphicon  glyphicon-arrow-left fa_ icon-arrow-left" aria-hidden="true"></i> {{_('Retour')}}</button></a>
                       <br>
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        {!! Form::model($bien, [
                            'method' => 'PATCH',
                            'route' => ['update_bien', $bien->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}           

                            @include ('apps.bien.form', ['formMode' => 'Mettre à jour'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
