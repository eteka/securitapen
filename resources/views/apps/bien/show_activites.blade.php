@extends('layouts.app')
@section('title')
   {{__('Activité')}} - {{$bien->nom." - "}}
    @parent
@endsection
@section('body')
    <body id="app-layout" class="bgwhite">
@show
@section('nav')
 @include('layouts.partials.simple-nav-dark')
 @include('layouts.partials.show_bien_topnav',['section'=>"activite"])
@endsection
@section('footer')
        @include('layouts.partials.footer-light')
@endsection

@section('content')
<style>
    .kt-widget2 .kt-widget2__item {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    margin-bottom: 1.4rem;
    position: relative;
}
.kt-widget2 .kt-widget2__item {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    margin-bottom: 1.4rem;
    position: relative;
}
.kt-portlet .kt-portlet__body {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    padding: 25px;
    border-radius: 4px;
}
.kt-portlet {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-shadow: 0px 0px 13px 0px rgba(82, 63, 105, 0.05);
    box-shadow: 0px 0px 13px 0px rgba(82, 63, 105, 0.05);
    background-color: #ffffff;
    margin-bottom: 20px;
    border-radius: 4px;
}
.kt-grid.kt-grid--hor:not(.kt-grid--desktop):not(.kt-grid--desktop-and-tablet):not(.kt-grid--tablet):not(.kt-grid--tablet-and-mobile):not(.kt-grid--mobile) {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
}
.kt-grid.kt-grid--ver:not(.kt-grid--desktop):not(.kt-grid--desktop-and-tablet):not(.kt-grid--tablet):not(.kt-grid--tablet-and-mobile):not(.kt-grid--mobile) {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-direction: row;
    flex-direction: row;
}
.kt-checkbox.kt-checkbox--single {
    width: 18px;
    height: 18px;
}

.kt-checkbox {
    display: inline-block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 10px;
    text-align: left;
    cursor: pointer;
    font-size: 1rem;
    -webkit-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
.kt-checkbox > input {
    position: absolute;
    z-index: -1;
    opacity: 0;
}
.kt-checkbox.kt-checkbox--solid > span {
    background: #e4e8ee;
    border: 1px solid transparent !important;
}

.kt-checkbox.kt-checkbox--single > span {
    top: 0px;
}
.kt-checkbox.kt-checkbox--solid > span {
    border: 1px solid transparent;
}
.kt-checkbox > span {
    border: 1px solid #d1d7e2;
}
.kt-checkbox > span {
    border-radius: 3px;
    background: none;
    position: absolute;
    top: 1px;
    left: 0;
    height: 18px;
    width: 18px;
}
.kt-widget2 .kt-widget2__item .kt-widget2__info {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    padding-left: 0.23rem 0 0 0.3rem;
}
.kt-widget2 .kt-widget2__item .kt-widget2__actions {
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    text-align: right;
    padding: 0 0.5rem 0 0.8rem;
    visibility: hidden;
}
.kt-widget2 .kt-widget2__item.kt-widget2__item--primary:before {
    background: #5867dd;
}.kt-widget2 .kt-widget2__item .kt-widget2__info .kt-widget2__title {
    font-weight: 500;
    margin: 0;
    color: #6c7293;
    -webkit-transition: color 0.3s ease;
    transition: color 0.3s ease;
}
.kt-widget2 .kt-widget2__item .kt-widget2__checkbox {
    padding: 1rem 0 0 2.2rem;
}

.kt-widget2 .kt-widget2__item .kt-widget2__info .kt-widget2__username {
    text-decoration: none;
    font-size: 0.9rem;
    color: #a7abc3;
    -webkit-transition: color 0.3s ease;
    transition: color 0.3s ease;
}

.kt-widget2 .kt-widget2__item:before {
    position: absolute;
    display: block;
    width: 0.3rem;
    border-radius: 4px;
    width: 4px;
    border-radius: 4px;
    height: 100%;
    left: 0.8rem;
    content: "";
}
.kt-widget2 .kt-widget2__item.kt-widget2__item--warning:before {
    background: #ffb822;
}
.kt-widget2 .kt-widget2__item.kt-widget2__item--brand:before {
    background: #3d94fb;
}
.kt-widget2 .kt-widget2__item.kt-widget2__item--success:before {
    background: #1dc9b7;
}
.kt-widget2 .kt-widget2__item.kt-widget2__item--danger:before {
    background: #fd27eb;
}
.kt-widget2 .kt-widget2__item.kt-widget2__item--info:before {
    background: #2786fb;
}
</style>
<div class="minh hmin">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="mtop15">
                    <div class="kt-widget2">
                        <div class="kt-widget2__item kt-widget2__item--primary">
                            <div class="kt-widget2__checkbox">
                                <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                                    <input type="checkbox">
                                    <span></span>
                                </label>
                            </div>
                            <div class="kt-widget2__info">
                                <a href="#" class="kt-widget2__title">
                                    Make Metronic Great  Again.Lorem Ipsum Amet 
                                </a>							 
                                <a href="#" class="kt-widget2__username">
                                    By Bob
                                </a>							 	 
                            </div>
                            <div class="kt-widget2__actions">
                                <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                    <i class="flaticon-more-1"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                    <ul class="kt-nav">
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                <span class="kt-nav__link-text">Reports</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-send"></i>
                <span class="kt-nav__link-text">Messages</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                <span class="kt-nav__link-text">Charts</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-avatar"></i>
                <span class="kt-nav__link-text">Members</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-settings"></i>
                <span class="kt-nav__link-text">Settings</span>
            </a>
        </li>
    </ul>							</div>
                            </div>
                        </div>
    
                        <div class="kt-widget2__item kt-widget2__item--warning">
                            <div class="kt-widget2__checkbox">
                                <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                                    <input type="checkbox">
                                    <span></span>
                                </label>
                            </div>
    
                            <div class="kt-widget2__info">
                                <a href="#" class="kt-widget2__title">
                                    Prepare Docs For Metting On Monday
                                </a>							 
                                <a href="#" class="kt-widget2__username">
                                    By Sean
                                </a>							 	 
                            </div>
    
                            <div class="kt-widget2__actions">
                                <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                    <i class="flaticon-more-1"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                    <ul class="kt-nav">
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                <span class="kt-nav__link-text">Reports</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-send"></i>
                <span class="kt-nav__link-text">Messages</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                <span class="kt-nav__link-text">Charts</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-avatar"></i>
                <span class="kt-nav__link-text">Members</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-settings"></i>
                <span class="kt-nav__link-text">Settings</span>
            </a>
        </li>
    </ul>							</div>
                            </div>						
                        </div>
    
                        <div class="kt-widget2__item kt-widget2__item--brand">
                            <div class="kt-widget2__checkbox">
                                <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                                <input type="checkbox">
                                <span></span>
                                </label>
                            </div>
    
                            <div class="kt-widget2__info">
                                <a href="#" class="kt-widget2__title">
                                    Make Widgets Great Again.Estudiat Communy Elit
                                </a>							 
                                <a href="#" class="kt-widget2__username">
                                    By Aziko
                                </a>							 	 
                            </div>
                            
                            <div class="kt-widget2__actions">
                                <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                    <i class="flaticon-more-1"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                    <ul class="kt-nav">
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                <span class="kt-nav__link-text">Reports</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-send"></i>
                <span class="kt-nav__link-text">Messages</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                <span class="kt-nav__link-text">Charts</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-avatar"></i>
                <span class="kt-nav__link-text">Members</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-settings"></i>
                <span class="kt-nav__link-text">Settings</span>
            </a>
        </li>
    </ul>							</div>
                            </div>
                        </div>
    
                        <div class="kt-widget2__item kt-widget2__item--success">
                            <div class="kt-widget2__checkbox">
                                <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                                <input type="checkbox">
                                <span></span>
                                </label>
                            </div>
    
                            <div class="kt-widget2__info">
                                <a href="#" class="kt-widget2__title">
                                    Make Metronic Great Again. Lorem Ipsum
                                </a>
                                <a class="kt-widget2__username">
                                    By James							 
                                </a>		 
                            </div>
                            <div class="kt-widget2__actions">
                                <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                    <i class="flaticon-more-1"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                    <ul class="kt-nav">
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                <span class="kt-nav__link-text">Reports</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-send"></i>
                <span class="kt-nav__link-text">Messages</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                <span class="kt-nav__link-text">Charts</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-avatar"></i>
                <span class="kt-nav__link-text">Members</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-settings"></i>
                <span class="kt-nav__link-text">Settings</span>
            </a>
        </li>
    </ul>							</div>
                            </div>
                        </div>
    
                        <div class="kt-widget2__item kt-widget2__item--danger">
                            <div class="kt-widget2__checkbox">
                                <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                                <input type="checkbox">
                                <span></span>
                                </label>
                            </div>
    
    
                            <div class="kt-widget2__info">
                                <a href="#" class="kt-widget2__title">
                                    Completa Financial Report For Emirates Airlines
                                </a>							 
                                <a href="#" class="kt-widget2__username">
                                    By Bob
                                </a>							 	 
                            </div>
                            
                            <div class="kt-widget2__actions">
                                <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                    <i class="flaticon-more-1"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                    <ul class="kt-nav">
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                <span class="kt-nav__link-text">Reports</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-send"></i>
                <span class="kt-nav__link-text">Messages</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                <span class="kt-nav__link-text">Charts</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-avatar"></i>
                <span class="kt-nav__link-text">Members</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-settings"></i>
                <span class="kt-nav__link-text">Settings</span>
            </a>
        </li>
    </ul>							</div>
                            </div>
                        </div>
    
                        <div class="kt-widget2__item kt-widget2__item--info">
                            <div class="kt-widget2__checkbox">
                                <label class="kt-checkbox kt-checkbox--solid kt-checkbox--single">
                                <input type="checkbox">
                                <span></span>
                                </label>
                            </div>
    
                            <div class="kt-widget2__info">
                                <a href="#" class="kt-widget2__title">
                                    Completa Financial Report For Emirates Airlines
                                <p></p>							 
                                </a><a href="#" class="kt-widget2__username">
                                    By Sean
                                </a>							 	 
                            </div>
                            
                            <div class="kt-widget2__actions">
                                <a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown">
                                    <i class="flaticon-more-1"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                    <ul class="kt-nav">
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                <span class="kt-nav__link-text">Reports</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-send"></i>
                <span class="kt-nav__link-text">Messages</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                <span class="kt-nav__link-text">Charts</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-avatar"></i>
                <span class="kt-nav__link-text">Members</span>
            </a>
        </li>
        <li class="kt-nav__item">
            <a href="#" class="kt-nav__link">
                <i class="kt-nav__link-icon flaticon2-settings"></i>
                <span class="kt-nav__link-text">Settings</span>
            </a>
        </li>
    </ul>							</div>
                            </div>
                        </div>
                    </div>
                </div>
                <p>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident nesciunt impedit est error quia natus dolores molestiae cumque blanditiis, repudiandae beatae nulla soluta quaerat odit porro neque. Facilis, aperiam temporibus.
                </p>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
@endsection