<div class="container">
    <div class="row">
        <div class="col-md-12">
            
<div id="notification">
    @section('notification') 
        @if(Session::has('flash_message') ) 
        <div class="alert padR0 mtop10__ alert-default alert-dismissible fade in" role="alert"> 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
            <strong>{{__('Information')}}</strong> 
            <p>{{ Session::get('flash_message') }} </p>
        </div>
        @endif
        @if(Session::has('info') ) 
        <div class="alert padR0 mtop10__ alert-info alert-dismissible fade in" role="alert"> 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
            <strong>{{__('Information')}}</strong> 
            <p>{{ Session::get('info') }} </p>
        </div>
        @endif
        @if(Session::has('success'))          
        <div class="alert mtop10__ alert-success alert-dismissible fade in" role="alert"> 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
            <strong>{{__('Opération réussie')}}</strong> 
            <p>{{ Session::get('success') }} </p>
        </div>
        @endif
        
        @if(Session::has('danger'))          
        <div class="alert mtop10__ alert-danger alert-dismissible fade in" role="alert"> 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
            <strong>{{__('Attention')}}</strong> 
            <p>{{ Session::get('danger') }} </p>
        </div>
        @endif
        @if(Session::has('warning'))          
        <div class="alert mtop10__ alert-warning alert-dismissible fade in" role="alert"> 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
            <strong>{{__('Avertissement')}}</strong> 
            <p>{{ Session::get('warning') }} </p>
        </div>

        @endif
    @show
    </div>
    
</div>
</div>
</div>