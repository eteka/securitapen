@if(Auth::user())
<div class="mtop10">
  <img class="img-responsive img-rounded mautos img-thumbnail" src="{{asset('storage/statics/users/image-profile.jpg')}}" alt="ETEKA Wilfried">
</div>
<div>
  <h5><a href="{{route('user_profil',Auth::user()->id)}}" class="bold">{{Auth::user()->name}}</a></h5>
  <ul class="list-unstyled menu-profil text-sm">
    @if(Auth::user()->poste)
    <li>
      <span class="sec_icon"><svg class="svg-icon" viewBox="0 0 20 20">
        <path d="M14.467,1.771H5.533c-0.258,0-0.47,0.211-0.47,0.47v15.516c0,0.414,0.504,0.634,0.802,0.331L10,13.955l4.136,4.133c0.241,0.241,0.802,0.169,0.802-0.331V2.241C14.938,1.982,14.726,1.771,14.467,1.771 M13.997,16.621l-3.665-3.662c-0.186-0.186-0.479-0.186-0.664,0l-3.666,3.662V2.711h7.994V16.621z"></path>
      </svg></span>
      {{Auth::user()->poste}}
    </li>
    @endif
    <li>
      <span class="sec_icon"><svg class="svg-icon" viewBox="0 0 20 20">
        <path d="M10,1.375c-3.17,0-5.75,2.548-5.75,5.682c0,6.685,5.259,11.276,5.483,11.469c0.152,0.132,0.382,0.132,0.534,0c0.224-0.193,5.481-4.784,5.483-11.469C15.75,3.923,13.171,1.375,10,1.375 M10,17.653c-1.064-1.024-4.929-5.127-4.929-10.596c0-2.68,2.212-4.861,4.929-4.861s4.929,2.181,4.929,4.861C14.927,12.518,11.063,16.627,10,17.653 M10,3.839c-1.815,0-3.286,1.47-3.286,3.286s1.47,3.286,3.286,3.286s3.286-1.47,3.286-3.286S11.815,3.839,10,3.839 M10,9.589c-1.359,0-2.464-1.105-2.464-2.464S8.641,4.661,10,4.661s2.464,1.105,2.464,2.464S11.359,9.589,10,9.589"></path>
      </svg></span>
      Parakou, Bénin
    </li><li>
      <span class="sec_icon"></span>
      <a href="https://securitapen.com/eteka-wilfried">https://securitapen.com</a>
    </li>
    <li>
      <span class="sec_icon"></span>
      <a href="https://securitapen.com/eteka-wilfried/badge">Badge Pro</a>
    </li>
    <li>
      <span class="sec_icon"></span>
      <a href="https://securitapen.com/eteka-wilfried/badge">Orgamisation</a>
    </li>
  </ul>
</div>
@endif
