<div class="row">

  <ol class="d-flex flex-wrap list-style-none gutter-condensed">

    @if(isset($biens) && $biens!=NULL && $biens->count()>0)

    @foreach($biens as $bien)
    <li class="col-12 col-md-6 col-lg-6 mb-3 d-flex flex-content-stretch blist">

      <div class="sec_card">
        <div class="card-body-sm">

          <div>
            <span class="plus_menu_cover" style="overflow: visible; position: relative; width: 80px;">
              <div class="dropdown">
                <a data-toggle="dropdown"  class="plus_menu" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                  <i class="icon-options"></i></a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <ul class="sec-nav">
                      <!--li class="sec-nav__item">
                      <a class="sec-nav__link" href="{{route('show_bien',$bien->id)}}">
                      <i class="icon icon-eye"></i>
                      <span class="sec-nav__link-text">Visualiser</span>
                    </a>
                  </li-->
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('edit_bien',$bien->id)}}">
                      <i class="icon-note"></i>
                      <span class="sec-nav__link-text">Modifier</span>
                    </a>
                  </li>
                  @if($bien->userFavoris && $bien->userFavoris->count())

                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('delete_bien_favori',$bien->id."?token=".sha1($bien->id))}}">
                      <i class="icon icon-star"></i>
                      <span class="sec-nav__link-text">{{__('Retirer de mes favoris')}}</span>
                    </a>
                  </li>
                  @else
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('add_bien_favori',$bien->id."?token=".sha1($bien->id))}}">
                      <i class="icon icon-star"></i>
                      <span class="sec-nav__link-text">{{__('Ajouter aux favoris')}}</span>
                    </a>
                  </li>

                  @endif
                  <li class="sec-nav__item" id="trash{{$bien->id}}">
                    <a class="sec-nav__link" type="button" data-toggle="modal" data-target="#SecDeleteModal{{$bien->id}}" data-whatever="@mdo" href="#trash{{$bien->id}}">

                      <i class="icon-trash"></i>
                      <span class="sec-nav__link-text">Supprimer</span>
                    </a>
                  </li>
                  <li class="sec-nav__item">
                    <a class="sec-nav__link" href="{{route('export_bien',$bien->id)}}">
                      <i class="icon-anc"></i>
                      <span class="sec-nav__link-text">Exporter</span>
                    </a>
                  </li>
                </ul>

              </div>
            </div>
          </span>
        </div>

        <div class="media mtop0">

          @if($bien->media && file_exists($bien->media->mini))
          @php
          $media=$bien->media;
          $url= $media['mini'];
          @endphp
          <div class="media-lefts pull-right mtop15" >
            <a href="{{route('show_bien',$bien->id)}}" ><img height='64px' class="media-circle" width="64px" src="{{asset($url)}}" alt="{{$media->title}}">  </a>
          </div>
          @endif

          <div class="media-body">
            <b class="media-heading "><a href="{{route('show_bien',$bien->id)}}">
              @if(Auth::user()->biensFavoris() && Auth::user()->biensFavoris()->where("bien_id",$bien->id)->count())
              <span class="title-svg-icon start-yellow" title="{{__('Ajouté aux favoris')}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/></svg>
              </span>
              @endif
              {{$bien->nom}}</a></b>
              <div class="pull-right"><span class="timeline-date">{{$bien->created_at->diffForHumans()}}</span>
              </div>
              <div class="bg-section_info">
                <ul class="list-inline">
                  @if($bien->imei1)
                  <li class="text-muted text-sm bgs rond3 bold">
                    <i class="icon-key"></i> <span itemprop="categorie">{{$bien->imei1}}</span>
                  </li>
                  @endif
                  @if($bien->mode)
                  <li class="text-muted text-sm bgm">
                    <i class="icon-anchor"></i> <span itemprop="categorie">{{$bien->mode->nom}}</span>
                  </li>
                  @endif
                    @if($bien->marque)
                      <li class="text-muted text-sm bgm">
                        <i class="icon-target"></i> <span itemprop="categorie">{{$bien->marque->nom}} {{$bien->modele?" - ".$bien->modele->nom:""}}</span>
                      </li>
                    @endif
                  @if($bien->date_achat)
                  <li class="text-muted text-sm bgm">
                    <i class="icon-calendar"></i> <span itemprop="categorie">{{$bien->date_achat}}</span>
                  </li>
                  @endif

                  <li class="text-muted text-sm bgm">
                    <i class="icon-location-pin"></i> <span itemprop="categorie">{{$bien->lieu_achat}}</span>
                  </li>

                  <li class="text-muted text-sm bgm">
                    <i class="icon-tag"></i> <span itemprop="categorie">{{afficherPrix($bien->prix_achat(),$bien->devise?$bien->devise->sigle:"",app()->getLocale())}}</span>
                  </li>

                </ul>

                <style>
                /*.modal-backdrop {
                opacity: 0.118;
                background: none;
                }*/
                </style>

                <div class="modal modal-danger fade" id="SecDeleteModal{{$bien->id}}" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <!--h4 class="modal-title text-uppercase" id="gridSystemModalLabel">
                        {{__("Supression d'élément")}} </h4-->
                        <h4 class="subhead-heading m0">{{__("Voulez-vous supprimer ce élément ?")}}</h4>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-12">
                            <span class="text-info"></span>
                            <form class="delete" action="{{route('delete_bien',$bien->id)}}" method="POST">
                              <input type="hidden" name="_method" value="DELETE">
                              {{ csrf_field() }}
                              <input type="hidden" name="deleted" id="deleted" value="{{sha1($bien->id)}}">


                              <div class="pad10 bordere rond5">

                                <h4>{{$bien->nom}}</h4>
                                <ul class="list-inline-point text-muted text-sm">
                                  <li>

                                    <small class=""><i class="icon-user"></i>  <a href=""> {{$bien->user?$bien->user->name:"-"}} </a> &nbsp;&nbsp;</small>
                                    <li>
                                      <small class=""><i class="icon-calendar"></i> {{__('Ajouté,')}} {{$bien->created_at->diffForHumans()}}</small>
                                    </li>
                                    @if($bien->created_at!=$bien->updated_at)
                                    <li>
                                      <small class=""><i class="icon-pencil"></i> {{__('Dernière mise à jour,')}} {{$bien->updated_at->diffForHumans()}}</small>
                                    </li>
                                    @endif
                                  </ul>
                                </div>
                                <div class="text-muted text-sm mtop5">
                                  {{__("Si vous remarquez que ce bien contient des erreurs, vous pouvez cliquez sur le bouton modifier")}}
                                  <a class="btn btn-xs pad0 _btn-outline-primary" href="{{route('edit_bien',$bien->id)}}"> <i class="icon-note"></i> {{__("Modifier")}}</a>
                                  <br>
                                </div>

                              </div>
                            </div>

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__("Fermer")}}</button>
                            <button type="submit" class="btn btn-danger"> <i class="icon-trash"></i> {{__("Supprimer")}}</button>

                          </form>
                        </div>
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div><!-- /.modal -->
                </div>




              </div>
            </div>
          </a>
        </div>


      </div>
    </li>
    @endforeach


  </ol>
  @else
  <div class="col-sm-12 col-md-12 col-md-offset- text-center">
    <div class="card sec_card ">
      <div class="card-body mtop30">
        <div class="pad30"> <i class="{{isset($icone)?$icone:"icon-layers"}} huge1 display-5"></i></div>
        <div class="mb20 pad10">{{isset($msg)?$msg:__("La boite magic de vos biens est vide")}}</div>
        <a href="{{route('create_bien')}}" class="btn btn-default btn-sm btn-blocks bold text-un mtop10"><i class="icon-plus "></i> {{__('Ajouter un nouveau')}}</a>
      </div>
    </div>


  </div>
  @endif
</div>

<div class="pagination-wrapper text-center">
  {{$biens && $biens->links()?$biens->links():''}}
</div>
