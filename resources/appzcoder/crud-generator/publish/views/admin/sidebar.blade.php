<div class="col-md-2 pr0 mtop10">
    @include("apps.includes.nav.mini-profil")
    <div class="slime_menu">
        <div class="">
            <ul class="nav slime_nav"  role="tablist">
                <li role="presentation">
                    <a href="{{ url('/admin') }}">
                    <i class="icon icon-home"></i>
                      {{__('Tableau de board')}}
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/posts') }}">
                    <i class="icon icon-share"></i>
                    {{__('Posts')}}
                    </a>
                </li>
                
            </ul>
        </div>
    </div>
</div>

