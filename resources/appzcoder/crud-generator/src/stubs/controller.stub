<?php

namespace DummyNamespace;

use DummyRootNamespaceHttp\Requests;
use DummyRootNamespaceHttp\Controllers\Controller;

use DummyRootNamespace{{modelNamespace}}{{modelName}};
use Illuminate\Http\Request;

class DummyClass extends Controller
{
    protected  $perPage = {{pagination}};
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $order=strtolower($request->get('orderBy'))=="asc"?"asc":"desc";
        $perPage = $this->perPage;
        $total={{modelName}}::select('id')->count();
        
        if (!empty($keyword)) {
            ${{crudName}} = {{modelName}}::{{whereSnippet}}orderBy("id",$order)->paginate($perPage);
        } else {
            ${{crudName}} = {{modelName}}::orderBy("id",$order)->paginate($perPage);
        }

        return view('{{viewPath}}{{viewName}}.index', compact('{{crudName}}','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('{{viewPath}}{{viewName}}.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        {{validationRules}}
        $requestData = $request->all();
        {{fileSnippet}}
        {{modelName}}::create($requestData);

        return redirect('{{routeGroup}}{{viewName}}')->with('success', __("Sauvegarde effectuée !"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        ${{crudNameSingular}} = {{modelName}}::findOrFail($id);

        return view('{{viewPath}}{{viewName}}.show', compact('{{crudNameSingular}}'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        ${{crudNameSingular}} = {{modelName}}::findOrFail($id);

        return view('{{viewPath}}{{viewName}}.edit', compact('{{crudNameSingular}}'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        {{validationRules}}
        $requestData = $request->all();
        {{fileSnippet}}
        ${{crudNameSingular}} = {{modelName}}::findOrFail($id);
        ${{crudNameSingular}}->update($requestData);

        return redirect('{{routeGroup}}{{viewName}}')->with('info', __("Mis à jour effectuée"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        {{modelName}}::destroy($id);

        return redirect('{{routeGroup}}{{viewName}}')->with('danger',  __("Suppression effectuée"));
    }
}
