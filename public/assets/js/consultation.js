/**
 * Created by UTILISATEUR on 27/07/2018.
 */
$(function () {
    $('.select2').select2();
    auto(".maladie");
    auto("#maladiep");
    autoExamen(".selectExamen");
    autoMedicament(".selectMedicament");
    autoCategorie(".selectCategorie");
//les détails de synthèse
    $(document).on('click', '#add_detail_synthese', function () {
        $('tbody#listedetailSynthese').append(
            '<tr class="listedetailSynthese">' +
            '<td class="col-md-6">{!! Form::select("selectCategorie",[],"", ["class" => "form-control select2 selectCategorie","style"=>"width: 100%; border:1px solid #ccc;padding: 5px 8px;"]) !!}' +
            '<td class="col-md-6">{!! Form::textarea("detail", null, ["class" => "form-control detail","rows"=>1]) !!}</td>' +
            '<td><span class="btn btn-default btn-xs  supprimerMDetail"> <i class=" fa fa-remove"></i></span></td>' +
            '</tr>'
        );
        $('.selectCategorie').select2();
        autoCategorie(".selectCategorie");
    });
    function autoCategorie(id) {
        $(id).select2({
                placeholder: "Saisir une catégorie",
                minimumInputLength: 1,
                ajax: {
                    url: '{{route("getAjaxCategorieExamen")}}', //&t=doctor
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        }
                            ;
                    }
                    ,
                    processResults: function (data) {
                        return {
                            results: data
                        }
                            ;
                    }
                    ,
                    cache: true
                }
            }
        );


    }

    $(document).on('click', "span.supprimerMDetail", function () {
        var ligne = $(this).closest('tr');
        ligne.remove();
    });
    function auto(id) {
        var s2 = $(id).select2({
                placeholder: "Saisir la maladie",
                minimumInputLength: 1,
                ajax: {
                    url: '{{route("getAjaxMaladie")}}', //&t=doctor
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        }
                            ;
                    }
                    ,
                    processResults: function (data) {
                        return {
                            results: data
                        }
                            ;
                    }
                    ,
                    cache: true
                }
            }
        );

        s2.on("change", function (e) {

            var select_val = $(e.currentTarget).val(),
                nb_elt = select_val.length;
            if (select_val !== null) {
                $('#add_row').removeClass('hidden').show().click();
            } else {
                $('#add_row').addClass('hidden').hide();
            }

            var data = $("#maladiep").select2('data'), textAfficher = '', txt;
            for (var i = 0; i < data.length; i++) {
                txt = data[i].text;
                if (txt == 'avec') {
                    txt = '<b>' + txt + '</b>';
                }
                if (txt == 'sur-le-terrain-de') {
                    txt = '<b>' + txt + '</b>';
                }
                if (txt == 'complique-de') {
                    txt = '<b>' + txt + '</b>';
                }
                textAfficher += txt + ' ';
            }
            $("#message").html(textAfficher)
//                alert(select_val);
            //alert.log(select_val)
        });
    }


    $("#add_row").on("click", function (e) {
        e.preventDefault();
        // Dynamic Rows Code
        //
        // Get max row id and set new id
        var newid = 0;
        $.each($("#tab_logic>div"), function () {
            if ($(this).hasClass("visible")) {
                newid++;
            }
        });
        newid++;
        if (isNaN(newid)) {
            newid = 1;
        }
        var table_linr_id = '#tmed' + newid, data = ($(table_linr_id).attr('data'));
        /*Premier essai*/
        if (newid <= 1) {
            $(table_linr_id).removeClass('hidden').addClass("visible").show();
            $(table_linr_id).attr("data-id", 1);
        } else { /*Second ou supperieur essai*/
            var nb_vide = 0;
            $.each($("#tab_logic .visible .dmal"), function () {

                var cur_td = $(this);
                var children = cur_td.children();
                if (children.val() === null) {
                    nb_vide++;
                }

            });
            if (nb_vide <= 0) {
                $(table_linr_id).removeClass('hidden').addClass("visible").show();
                $(table_linr_id).attr("data-id", 1);
            }
        }


    });

    $("#tab_logic").find("a.remove-link ,a.supline.fa-remove").on("click", function (e) {
        e.preventDefault();
        var id = $(this).attr('rel'), sv = $(this).attr('data-value');
        $('#' + sv).val(null).trigger("change");
//            $('#' + id).removeClass('visible').addClass("hidden");
    });
// Sortable Code
    var fixHelperModified = function (e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function (index) {
            $(this).width($originals.eq(index).width())
        });
        return $helper;
    };
    $("#add_row").trigger("click");

//les examen paracliniques
    $(document).on('click', '#add_examen', function () {
        $('tbody#listeExamen').append(
            '<tr class="listeExamen">' +
            '<td class="col-md-11"><select name="selectExamen" style="width: 100%; border:1px solid #ccc;padding: 5px 8px;" class="form-control select2 selectExamen"></select></td>' +
            '<td><span class="btn btn-default btn-xs  supprimerExamen"> <i class=" fa fa-remove"></i></span></td>' +
            '</tr>'
        );
        $('.selectExamen').select2();
        autoExamen(".selectExamen");
    });
    $(document).on('click', "span.supprimerExamen", function () {
        var ligne = $(this).closest('tr');
        ligne.remove();
    });
    function autoExamen(id) {
        $(id).select2({
                placeholder: "Saisir l'analyse",
                minimumInputLength: 1,
                ajax: {
                    url: '{{route("getAjaxAnalyse")}}', //&t=doctor
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        }
                            ;
                    }
                    ,
                    processResults: function (data) {
                        return {
                            results: data
                        }
                            ;
                    }
                    ,
                    cache: true
                }
            }
        );


    }

//les ordonnances
    $(document).on('click', '#add_medicament', function () {
        $('tbody#listeMedicament').append(
            '<tr class="listeMedicament">' +
            '<td class="col-md-6">{!! Form::select("selectMedicament",[],"", ["class" => "form-control select2 selectMedicament","style"=>"width: 100%; border:1px solid #ccc;padding: 5px 8px;"]) !!}' +
            '<td class="col-md-6">{!! Form::textarea("medoc", null, ["class" => "form-control posologie","rows"=>1]) !!}</td>' +
            '<td><span class="btn btn-default btn-xs  supprimerMedicament"> <i class=" fa fa-remove"></i></span></td>' +
            '</tr>'
        );
        $('.selectMedicament').select2();
        autoMedicament(".selectMedicament");
    });
    function autoMedicament(id) {
        $(id).select2({
                placeholder: "Saisir un médicament",
                minimumInputLength: 1,
                ajax: {
                    url: '{{route("getAjaxMedicament")}}', //&t=doctor
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        }
                            ;
                    }
                    ,
                    processResults: function (data) {
                        return {
                            results: data
                        }
                            ;
                    }
                    ,
                    cache: true
                }
            }
        );


    }

    $(document).on('click', "span.supprimerMedicament", function () {
        var ligne = $(this).closest('tr');
        ligne.remove();
    });
    $(document).on('change', 'select#hospitaliser', function () {
        if ($(this).val() == 1) {
            $("#rendez-vous").val('');
            $("#rendez-vous").hide();
        }
        else
            $("#rendez-vous").show();

    })
});