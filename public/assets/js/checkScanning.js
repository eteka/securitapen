function AfficheInfoScan(code)
{
    $('div#content_info_scan').html('');
    $.ajax({
        url: _url+"/scanForm",
        type:'post',
        data:{code:code},
        success: function (data) {
            $('div#content_info_scan').html(data);
            $('div#m_modal_scan_info').modal('show');
        }
    });
}
function executeQuery() {

    if($('input#scan_patient_id').val()==='')
    {
        $.ajax({
            url: _url+"/checkScan",
            data:{refresh:1},
            dataType	: 'json',
            success: function (data) {
                if(data.reponse==1)
                {
                    AfficheInfoScan(data.code);
                }
                if(data.reponse==2)
                {
                    $('div#content_info_scan').append('<form action="'+_url+'/admin/patient/scanInscription'+'" method="post" id="form">' +
                        '<input type="hidden" name="_token" value='+_tocken+'><input type="hidden" name="code" value='+data.code+'> </form> ');

                    $("form#form").submit();

                }
            }
        });

    }setTimeout(executeQuery, 5000);
    // you could choose not to continue on failure...
}

$(document).ready(function() {
    // run the first time; all subsequent calls will take care of themselves
    setTimeout(executeQuery, 5000);
});

$(document).on('click','button.fermerModal',function(){
    $('div#content_info_scan').html('<input class="form-control" type="hidden"   id="scan_patient_id" value="" />');
    $('div#m_modal_scan_info').modal('hide');
})

$(document).on('click','a.billing',function () {
    $(this).append('<form action="'+_url+'/centre/caisse/addFacture'+'" method="post" id="billing">' +
        ' <input type="hidden" name="_token" value='+_tocken+'><input type="hidden" name="id" value='+$(this).attr('data-id')+'> </form> ');
    $("form#billing").submit();
});
$(document).on('click','a.consulter',function(){
    $(this).append('<form action="'+_url+'/admission'+'" method="post" id="form">' +
        '<input type="hidden" name="_token" value='+_tocken+'><input type="hidden" name="username" value='+$(this).attr('data-id')+'> </form> ');

    $("form#form").submit();
})

$(document).on('click','a.constante',function(){
    $(this).append('<form action="'+_url+'/patient/preconsultation/create'+'"  method="post" id="form">' +
        '<input type="hidden" name="_token" value='+_tocken+'><input type="hidden" name="id" value='+$(this).attr('data-id')+'> </form> ');

    $("form#form").submit();
})